//
//  restFoodModelTests.swift
//  calorie-counterTests
//
//  Created by Michał Mańkus on 01/02/2022.
//

import XCTest
@testable import calorie_counter

class RestFoodModelTests: XCTestCase {
    
    func test_givenAddFoodEntry_thenResultIsCorrect() {
        // GIVEN
        let foodGatewayStub = FoodGatewayStub()
        let sut = RESTFoodModel(foodGateway: foodGatewayStub)
        
        foodGatewayStub.addFoodEntryCompletionResult = .success(())
        
        // WHEN
        sut.addFoodEntry(date: Date(), name: "Food", calories: 150, price: 5.0) { _ in }
        
        // THEN
        XCTAssertEqual(foodGatewayStub.addFoodEntryCallsCount, 1)
    }
    
    func test_givenGatewayResponse_thenModelResponseIsCorrect() {
        // GIVEN
        let foodGatewayStub = FoodGatewayStub()
        let sut = RESTFoodModel(foodGateway: foodGatewayStub)
        
        // WHEN
        
        var components = Calendar.current.dateComponents([.day, .year, .month], from: Date())
        components.hour = 12
        components.minute = 0
        components.second = 0
        let baseMiddayDate = Calendar.current.date(from: components)!
        
        let entryA = FoodEntryResponse(
            id: "1",
            creatorId: "c1",
            creatorName: "user1",
            name: "Food 1",
            calories: 100,
            price: nil,
            createdAt: baseMiddayDate
        )
        let entryB = FoodEntryResponse(
            id: "2",
            creatorId: "c1",
            creatorName: "user1",
            name: "Food 2",
            calories: 200,
            price: nil,
            createdAt: baseMiddayDate.addingTimeInterval(86400)
        )
        let entryC = FoodEntryResponse(
            id: "3",
            creatorId: "c1",
            creatorName: "user1",
            name: "Food 3",
            calories: 300,
            price: nil,
            createdAt: baseMiddayDate.addingTimeInterval(-86400)
        )
        let entries = [entryA, entryB, entryC]
        foodGatewayStub.getFoodEntryCompletionResult = .success(.init(result: entries))
        
        // THEN
        sut.getFoodEntries(fromDate: Date(), toDate: Date()) { result in
            switch result {
            case .success(let dayEntriesResult):
                XCTAssertEqual(dayEntriesResult.count, 1)
                XCTAssertEqual(dayEntriesResult[0].entries.count, 1)
                XCTAssertEqual(dayEntriesResult[0].entries[0], FoodEntryData(from: entryA))
            case .failure:
                XCTFail("Should be a success")
            }
        }
        
        XCTAssertEqual(foodGatewayStub.getFoodEntryCallsCount, 1)
    }
    
    func test_givenGatewayResponse_thenModelResponseIsCorrect2() {
        // GIVEN
        let foodGatewayStub = FoodGatewayStub()
        let sut = RESTFoodModel(foodGateway: foodGatewayStub)
        
        // WHEN
        
        var components = Calendar.current.dateComponents([.day, .year, .month], from: Date())
        components.hour = 12
        components.minute = 0
        components.second = 0
        let baseMiddayDate = Calendar.current.date(from: components)!
        
        let entryA = FoodEntryResponse(
            id: "1",
            creatorId: "c1",
            creatorName: "user1",
            name: "Food 1",
            calories: 100,
            price: nil,
            createdAt: baseMiddayDate
        )
        let entryB = FoodEntryResponse(
            id: "2",
            creatorId: "c1",
            creatorName: "user1",
            name: "Food 2",
            calories: 200,
            price: nil,
            createdAt: baseMiddayDate.addingTimeInterval(86400)
        )
        let entryC = FoodEntryResponse(
            id: "3",
            creatorId: "c1",
            creatorName: "user1",
            name: "Food 3",
            calories: 300,
            price: nil,
            createdAt: baseMiddayDate.addingTimeInterval(-86400)
        )
        let entries = [entryA, entryB, entryC]
        foodGatewayStub.getFoodEntryCompletionResult = .success(.init(result: entries))
        
        // THEN
        sut.getFoodEntries(
            fromDate: Date().addingTimeInterval(-200_000),
            toDate: Date().addingTimeInterval(200_000)
        ) { result in
            switch result {
            case .success(let dayEntriesResult):
                XCTAssertEqual(dayEntriesResult.count, 3)
                XCTAssertEqual(dayEntriesResult[0].entries.count, 1)
                XCTAssertEqual(dayEntriesResult[0].entries[0], FoodEntryData(from: entryB))
            case .failure:
                XCTFail("Should be a success")
            }
        }
        
        XCTAssertEqual(foodGatewayStub.getFoodEntryCallsCount, 1)
    }
    
    func test_givenGatewayResponse_thenModelResponseIsCorrect3() {
        // GIVEN
        let foodGatewayStub = FoodGatewayStub()
        let sut = RESTFoodModel(foodGateway: foodGatewayStub)
        
        // WHEN
        
        var components = Calendar.current.dateComponents([.day, .year, .month], from: Date())
        components.hour = 12
        components.minute = 0
        components.second = 0
        let baseMiddayDate = Calendar.current.date(from: components)!
        
        let entryA = FoodEntryResponse(
            id: "1",
            creatorId: "c1",
            creatorName: "user1",
            name: "Food 1",
            calories: 100,
            price: nil,
            createdAt: baseMiddayDate
        )
        let entryB = FoodEntryResponse(
            id: "2",
            creatorId: "c1",
            creatorName: "user1",
            name: "Food 2",
            calories: 200,
            price: nil,
            createdAt: baseMiddayDate.addingTimeInterval(-3600)
        )
        let entryC = FoodEntryResponse(
            id: "3",
            creatorId: "c1",
            creatorName: "user1",
            name: "Food 3",
            calories: 300,
            price: nil,
            createdAt: baseMiddayDate.addingTimeInterval(-7200)
        )
        let entries = [entryA, entryB, entryC]
        foodGatewayStub.getFoodEntryCompletionResult = .success(.init(result: entries))
        
        // THEN
        sut.getFoodEntries(
            fromDate: Date().addingTimeInterval(-200_000),
            toDate: Date().addingTimeInterval(200_000)
        ) { result in
            switch result {
            case .success(let dayEntriesResult):
                XCTAssertEqual(dayEntriesResult.count, 1)
                XCTAssertEqual(dayEntriesResult[0].entries.count, 3)
                XCTAssertEqual(dayEntriesResult[0].entries[0], FoodEntryData(from: entryA))
                XCTAssertEqual(dayEntriesResult[0].totalCalories, 600)
            case .failure:
                XCTFail("Should be a success")
            }
        }
        
        XCTAssertEqual(foodGatewayStub.getFoodEntryCallsCount, 1)
    }
    
    func test_givenGatewayResponse_thenMonthlySpendingIsCorrect() {
        // GIVEN
        let foodGatewayStub = FoodGatewayStub()
        let sut = RESTFoodModel(foodGateway: foodGatewayStub)
        
        // WHEN
        
        let date = Date(timeIntervalSince1970: 1642244400) // 15-01-2022 12:00:00
        
        let entryA = FoodEntryResponse(
            id: "1",
            creatorId: "c1",
            creatorName: "user1",
            name: "Food 1",
            calories: 100,
            price: 5,
            createdAt: date
        )
        let entryB = FoodEntryResponse(
            id: "2",
            creatorId: "c1",
            creatorName: "user1",
            name: "Food 2",
            calories: 200,
            price: 8,
            createdAt: date.addingTimeInterval(86400 * 5) // 20-01-2022
        )
        let entryC = FoodEntryResponse(
            id: "3",
            creatorId: "c1",
            creatorName: "user1",
            name: "Food 3",
            calories: 300,
            price: 2,
            createdAt: date.addingTimeInterval(-86400 * 20) // 25-12-2021
        )
        let entries = [entryA, entryB, entryC]
        foodGatewayStub.getFoodEntryCompletionResult = .success(.init(result: entries))
        
        // THEN
        sut.getFoodEntries(fromDate: nil, toDate: nil) { result in
            switch result {
            case .success(let dayEntriesResult):
                XCTAssertEqual(dayEntriesResult.count, 3)
                XCTAssertEqual(dayEntriesResult[0].entries.count, 1)
                XCTAssertEqual(dayEntriesResult[0].entries[0], FoodEntryData(from: entryB))
                XCTAssertEqual(dayEntriesResult[0].monthlyMoneySpent, 13)
                XCTAssertEqual(dayEntriesResult[2].monthlyMoneySpent, 2)
            case .failure:
                XCTFail("Should be a success")
            }
        }
        
        XCTAssertEqual(foodGatewayStub.getFoodEntryCallsCount, 1)
    }
}
