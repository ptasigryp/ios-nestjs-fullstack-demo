//
//  FoodEntryDataTests.swift
//  calorie-counterTests
//
//  Created by Michał Mańkus on 01/02/2022.
//

import XCTest
@testable import calorie_counter

class FoodEntryDataTests: XCTestCase {
    
    func test_givenFoodEntryPrice_thenPriceStringIsCorrect() {
        // GIVEN
        let foodData = FoodEntryData(
            id: "1",
            creatorId: "c1",
            creatorName: "user",
            date: Date(),
            productName: "kiwi",
            kcal: 100,
            price: 15.01001
        )
        
        // WHEN
        let priceString = foodData.priceString
        
        // THEN
        XCTAssertEqual(priceString, "15.01")
    }
}
