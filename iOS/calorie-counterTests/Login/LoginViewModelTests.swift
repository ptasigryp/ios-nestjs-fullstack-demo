//
//  loginViewModelTests.swift
//  loginViewModelTests
//
//  Created by Michał Mańkus on 29/01/2022.
//

import XCTest
@testable import calorie_counter

class LoginViewModelTests: XCTestCase {

    func test_givenEmptyCredentials_whenLoggingIn_thenLoginModelWasntCalled() {
        // GIVEN
        let accountModelStub = AccountModelStub()
        let sut = LoginViewModel(accountModel: accountModelStub)
        
        // WHEN
        sut.login(username: "", password: "")
        
        // THEN
        XCTAssertEqual(accountModelStub.loginCallsCount, 0)
    }

    func test_givenEmptyPassword_whenLoggingIn_thenLoginModelWasntCalled() {
        // GIVEN
        let accountModelStub = AccountModelStub()
        let sut = LoginViewModel(accountModel: accountModelStub)
        
        // WHEN
        sut.login(username: "username", password: "")
        
        // THEN
        XCTAssertEqual(accountModelStub.loginCallsCount, 0)
    }
    
    func test_givenEmptyUsername_whenLoggingIn_thenLoginModelWasntCalled() {
        // GIVEN
        let accountModelStub = AccountModelStub()
        let sut = LoginViewModel(accountModel: accountModelStub)
        
        // WHEN
        sut.login(username: "", password: "password")
        
        // THEN
        XCTAssertEqual(accountModelStub.loginCallsCount, 0)
    }
    
    func test_givenCorrectCredentials_whenLoggingIn_thenLoginModelWasCalled() {
        // GIVEN
        let accountModelStub = AccountModelStub()
        let sut = LoginViewModel(accountModel: accountModelStub)
        
        // WHEN
        sut.login(username: "username", password: "password")
        
        // THEN
        XCTAssertEqual(accountModelStub.loginCallsCount, 1)
    }
}
