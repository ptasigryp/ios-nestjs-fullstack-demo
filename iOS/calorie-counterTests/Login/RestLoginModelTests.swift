//
//  restLoginModelTests.swift
//  calorie-counterTests
//
//  Created by Michał Mańkus on 29/01/2022.
//

import XCTest
@testable import calorie_counter

class RestLoginModelTests: XCTestCase {

    func test_givenSuccessfulLoginAndAccountDetailsCall_whenLoggingIn_thenResultIsCorrect() {
        // GIVEN
        let userState = UserState()
        let authGatewayStub = AuthGatewayStub()
        let accountGatewayStub = AccountGatewayStub()
        let sut = RESTAccountModel(
            userState: userState,
            authGateway: authGatewayStub,
            accountGateway: accountGatewayStub
        )
        
        let accessToken = "abcefgasdasd"
        let accountDetails = AccountDetailsResponse(
            username: "michael",
            isAdmin: true
        )
        authGatewayStub.loginCompletionResult = .success(
            .init(token: accessToken)
        )
        accountGatewayStub.getAccountDetailsCompletionResult = .success(accountDetails)
        
        // WHEN
        sut.login(username: "", password: "") { _ in }
        
        // THEN
        XCTAssertEqual(authGatewayStub.loginCallsCount, 1)
        XCTAssertEqual(accountGatewayStub.getAccountDetailsCallsCount, 1)
        XCTAssertEqual(authGatewayStub.token, accessToken)
        XCTAssertEqual(userState.username, accountDetails.username)
        XCTAssertEqual(userState.isAdmin, accountDetails.isAdmin)
    }
    
    func test_givenUnsuccessfulLogin_whenLoggingIn_thenTokenAndAccountDataAreEmpty() {
        // GIVEN
        let userState = UserState()
        let authGatewayStub = AuthGatewayStub()
        let accountGatewayStub = AccountGatewayStub()
        let sut = RESTAccountModel(
            userState: userState,
            authGateway: authGatewayStub,
            accountGateway: accountGatewayStub
        )
        
        authGatewayStub.loginCompletionResult = .failure(AppError.unknown)
        
        // WHEN
        sut.login(username: "", password: "") { _ in }
        
        // THEN
        XCTAssertEqual(authGatewayStub.loginCallsCount, 1)
        XCTAssertEqual(accountGatewayStub.getAccountDetailsCallsCount, 0)
        XCTAssertEqual(authGatewayStub.token, "")
        XCTAssertEqual(userState.username, nil)
        XCTAssertEqual(userState.isAdmin, false)
    }
    
    func test_givenUnsuccessfulAccountDetails_whenLoggingIn_thenAccountDataIsEmpty() {
        // GIVEN
        let userState = UserState()
        let authGatewayStub = AuthGatewayStub()
        let accountGatewayStub = AccountGatewayStub()
        let sut = RESTAccountModel(
            userState: userState,
            authGateway: authGatewayStub,
            accountGateway: accountGatewayStub
        )
        
        authGatewayStub.loginCompletionResult = .success(
            .init(token: "abcegasdasd")
        )
        
        // WHEN
        sut.login(username: "", password: "") { _ in }
        
        // THEN
        XCTAssertEqual(authGatewayStub.loginCallsCount, 1)
        XCTAssertEqual(accountGatewayStub.getAccountDetailsCallsCount, 1)
        XCTAssertEqual(authGatewayStub.token, "abcegasdasd")
        XCTAssertEqual(userState.username, nil)
        XCTAssertEqual(userState.isAdmin, false)
    }
}
