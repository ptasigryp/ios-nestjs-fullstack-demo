//
//  storageManagerTests.swift
//  calorie-counterTests
//
//  Created by Michał Mańkus on 29/01/2022.
//

import XCTest
@testable import calorie_counter

class StorageManagerTests: XCTestCase {
    
    func test_givenStorageManager_whenSettingValue_thenCorrectValueIsRetrieved() {
        // GIVEN
        let sut = StorageManager()
        
        // WHEN
        sut.set(1200, for: .calorieLimit)
        
        // THEN
        XCTAssertEqual(sut.int(for: .calorieLimit), 1200)
    }
    
    func test_givenStorageManager_whenSettingValue_thenCorrectValueIsRetrieved2() {
        // GIVEN
        let sut = StorageManager()
        
        // WHEN
        sut.set(2100, for: .calorieLimit)
        
        // THEN
        XCTAssertEqual(sut.int(for: .calorieLimit), 2100)
    }
}
