//
//  userSettingsManagerTests.swift
//  calorie-counterTests
//
//  Created by Michał Mańkus on 29/01/2022.
//

import XCTest
@testable import calorie_counter

class SserSettingsManagerTests: XCTestCase {

    func test_givenEmptyUserSettings_whenRetrievingCaloriesLimit_thenDefaultValueIs2100() {
        // GIVEN
        let storageManagerStub = StorageManagerStub()
        let sut = UserSettingsManager(storageManager: storageManagerStub)
        storageManagerStub.storageValues[.calorieLimit] = nil
        
        // WHEN
        let limit = sut.caloriesLimit
        
        // THEN
        XCTAssertEqual(limit, 2100)
    }
    
    func test_givenUserSettings_whenSettingCaloriesLimit_thenCorrectValueIsRetrieved() {
        // GIVEN
        let storageManagerStub = StorageManagerStub()
        let sut = UserSettingsManager(storageManager: storageManagerStub)
        
        // WHEN
        sut.caloriesLimit = 3000
        
        // THEN
        XCTAssertEqual(sut.caloriesLimit, 3000)
    }
}
