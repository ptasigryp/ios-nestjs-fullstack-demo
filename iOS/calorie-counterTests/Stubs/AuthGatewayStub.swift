//
//  AuthGatewayStub.swift
//  calorie-counterTests
//
//  Created by Michał Mańkus on 29/01/2022.
//

import Foundation
@testable import calorie_counter

final class AuthGatewayStub: AuthGatewayClient {
    
    private(set) var updateAccessTokenCallsCount = 0
    private(set) var token = ""
    
    func updateAccessToken(_ token: String) {
        updateAccessTokenCallsCount += 1
        self.token = token
    }
    
    private(set) var clearAccessTokenCallsCount = 0
    
    func clearAccessToken() {
        clearAccessTokenCallsCount += 1
        self.token = ""
    }
    
    private(set) var loginCallsCount = 0
    var loginCompletionResult: Result<LoginDetailsResponse, Error> = .failure(AppError.unknown)
    
    func login(username: String, password: String, completion: @escaping NetworkHandler<LoginDetailsResponse>) {
        loginCallsCount += 1
        completion(loginCompletionResult)
    }
    
}
