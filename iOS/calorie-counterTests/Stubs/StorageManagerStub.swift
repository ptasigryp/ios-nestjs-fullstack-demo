//
//  StorageManagerStub.swift
//  calorie-counterTests
//
//  Created by Michał Mańkus on 29/01/2022.
//

import Foundation
@testable import calorie_counter

final class StorageManagerStub: StorageManagerProtocol {
    
    var storageValues: [StorageKey: Any?] = [
        .calorieLimit: nil
    ]
    
    func set(_ value: Int, for key: StorageKey) {
        storageValues[key] = value
    }
    
    func int(for key: StorageKey) -> Int {
        guard let value = storageValues[key] else { return 0 }
        return value as? Int ?? 0
    }
}
