//
//  AccountGatewayStub.swift
//  calorie-counterTests
//
//  Created by Michał Mańkus on 29/01/2022.
//

import Foundation
@testable import calorie_counter

final class AccountGatewayStub: AccountGatewayClient {
    
    private(set) var getAccountDetailsCallsCount = 0
    var getAccountDetailsCompletionResult: Result<AccountDetailsResponse, Error> = .failure(AppError.unknown)
    
    func getAccountDetails(completion: @escaping NetworkHandler<AccountDetailsResponse>) {
        getAccountDetailsCallsCount += 1
        completion(getAccountDetailsCompletionResult)
    }
}
