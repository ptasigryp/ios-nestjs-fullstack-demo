//
//  FoodGatewayStub.swift
//  calorie-counterTests
//
//  Created by Michał Mańkus on 01/02/2022.
//

import Foundation
@testable import calorie_counter

final class FoodGatewayStub: FoodGatewayClient {
    
    private(set) var addFoodEntryCallsCount = 0
    var addFoodEntryCompletionResult: Result<Void, Error> = .success(())
    
    func addFoodEntry(
        date: Date,
        name: String,
        calories: Int,
        price: Double?,
        completion: @escaping EmptyNetworkHandler
    ) {
        addFoodEntryCallsCount += 1
        completion(addFoodEntryCompletionResult)
    }
    
    
    private(set) var updateFoodEntryCallsCount = 0
    var updateFoodEntryCompletionResult: Result<Void, Error> = .success(())
    
    func updateFoodEntry(
        id: String,
        date: Date,
        name: String,
        calories: Int,
        price: Double?,
        completion: @escaping EmptyNetworkHandler
    ) {
        updateFoodEntryCallsCount += 1
        completion(updateFoodEntryCompletionResult)
    }
    
    private(set) var getFoodEntryCallsCount = 0
    var getFoodEntryCompletionResult: Result<FoodEntriesResponse, Error> = .failure(AppError.unknown)
    
    func getFoodEntries(completion: @escaping NetworkHandler<FoodEntriesResponse>) {
        getFoodEntryCallsCount += 1
        completion(getFoodEntryCompletionResult)
    }
    
    private(set) var removeFoodEntryCallsCount = 0
    var removeFoodEntryCompletionResult: Result<Void, Error> = .success(())
    
    func removeFoodEntry(id: String, completion: @escaping EmptyNetworkHandler) {
        removeFoodEntryCallsCount += 1
        completion(removeFoodEntryCompletionResult)
    }
}
