//
//  AccountModelStub.swift
//  calorie-counterTests
//
//  Created by Michał Mańkus on 29/01/2022.
//

import Foundation
@testable import calorie_counter

final class AccountModelStub: AccountModel {
    
    private(set) var loginCallsCount = 0
    var loginCompletionResult: Result<Void, Error> = .success(())
    
    func login(username: String, password: String, completion: @escaping EmptyNetworkHandler) {
        loginCallsCount += 1
        completion(loginCompletionResult)
    }
    
    private(set) var logoutCallsCount = 0
    
    func logout() {
        loginCallsCount += 1
    }
}
