//
//  FoodEntryData.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import Foundation

struct FoodEntryData: Equatable {
    
    let id: String
    let creatorId: String
    let creatorName: String
    let date: Date
    let productName: String
    let kcal: Int
    let price: Double?
    
    var priceString: String? {
        guard let price = price else { return nil }
        return String(format: "%.2f", price)
    }
    
    init(
        id: String,
        creatorId: String,
        creatorName: String,
        date: Date,
        productName: String,
        kcal: Int,
        price: Double? = nil
    ) {
        self.id = id
        self.creatorId = creatorId
        self.creatorName = creatorName
        self.date = date
        self.productName = productName
        self.kcal = kcal
        self.price = price
    }
    
    init(from response: FoodEntryResponse) {
        self.id = response.id
        self.creatorId = response.creatorId
        self.creatorName = response.creatorName
        self.date = response.createdAt
        self.productName = response.name
        self.kcal = response.calories
        self.price = response.price
    }
}
