//
//  FoodDayEntryData.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 30/01/2022.
//

import Foundation

struct FoodDayEntryData {
    
    let date: Date
    let entries: [FoodEntryData]
    var monthlyMoneySpent: Double = 0.0
    
    var totalCalories: Int {
        entries.reduce(0, { $0 + $1.kcal })
    }
    
    var monthlyMoneySpentString: String {
        String(format: "%.2f", monthlyMoneySpent)
    }
}
