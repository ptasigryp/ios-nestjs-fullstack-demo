//
//  ModelFactory.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import Foundation

enum Model {
    
    enum Mode {
        
        case mock
        case rest
    }
    
    static var factory: ModelFactory = RESTModelFactory()
    
    static func setMode(_ mode: Mode) {
        switch mode {
        case .rest:
            Self.factory = RESTModelFactory()
        case .mock:
            Self.factory = MockModelFactory()
        }
    }
}

protocol ModelFactory {
    
    var accountModel: AccountModel { get }
    var foodModel: FoodModel { get }
}

final class RESTModelFactory: ModelFactory {
    
    private(set) lazy var accountModel: AccountModel = RESTAccountModel()
    private(set) lazy var foodModel: FoodModel = RESTFoodModel()
}

final class MockModelFactory: ModelFactory {
    
    private(set) lazy var accountModel: AccountModel = MockAccountModel()
    private(set) lazy var foodModel: FoodModel = MockFoodModel()
}
