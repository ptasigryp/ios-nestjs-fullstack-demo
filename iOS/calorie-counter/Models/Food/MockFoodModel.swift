//
//  MockFoodModel.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 30/01/2022.
//

import Foundation

final class MockFoodModel: FoodModel {
    
    func addFoodEntry(
        date: Date,
        name: String,
        calories: Int,
        price: Double?,
        completion: @escaping EmptyNetworkHandler
    ) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            completion(.success(()))
        }
    }
    
    func updateFoodEntry(
        id: String,
        date: Date,
        name: String,
        calories: Int,
        price: Double?,
        completion: @escaping EmptyNetworkHandler
    ) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            completion(.success(()))
        }
    }

    func getFoodEntries(
        fromDate: Date? = nil,
        toDate: Date? = nil,
        completion: @escaping NetworkHandler<[FoodDayEntryData]>
    ) {
        let result: [FoodDayEntryData] = [
            .init(date: Date(), entries: [
                .init(
                    id: "1",
                    creatorId: "abasdasd",
                    creatorName: "User",
                    date: Date(),
                    productName: "Banana",
                    kcal: 120
                ),
                .init(
                    id: "5",
                    creatorId: "abasdasd",
                    creatorName: "User",
                    date: Date(),
                    productName: "Orange juice",
                    kcal: 230
                )
            ]),
            .init(date: Date().addingTimeInterval(-150000), entries: [
                .init(
                    id: "55",
                    creatorId: "abasdasd",
                    creatorName: "User",
                    date: Date().addingTimeInterval(-150000),
                    productName: "Chocolate",
                    kcal: 550
                ),
            ])
        ]
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            completion(.success(result))
        }
    }
    
    func removeFoodEntry(id: String, completion: @escaping EmptyNetworkHandler) {
        completion(.success(()))
    }
    
    func getAdminStats(completion: @escaping NetworkHandler<FoodAdminStatsResponse>) {
        completion(.failure(AppError.unknown))
    }
}
