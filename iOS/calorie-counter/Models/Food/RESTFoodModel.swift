//
//  RESTFoodModel.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 30/01/2022.
//

import Foundation

final class RESTFoodModel: FoodModel {
    
    private let foodGateway: FoodGatewayClient
    
    init(
        foodGateway: FoodGatewayClient = GatewayClient.shared
    ) {
        self.foodGateway = foodGateway
    }
    
    func addFoodEntry(
        date: Date,
        name: String,
        calories: Int,
        price: Double?,
        completion: @escaping EmptyNetworkHandler
    ) {
        foodGateway.addFoodEntry(
            date: date,
            name: name,
            calories: calories,
            price: price,
            completion: completion
        )
    }
    
    func updateFoodEntry(
        id: String,
        date: Date,
        name: String,
        calories: Int,
        price: Double?,
        completion: @escaping EmptyNetworkHandler
    ) {
        foodGateway.updateFoodEntry(
            id: id,
            date: date,
            name: name,
            calories: calories,
            price: price,
            completion: completion
        )
    }
    
    func getFoodEntries(
        fromDate: Date? = nil,
        toDate: Date? = nil,
        completion: @escaping NetworkHandler<[FoodDayEntryData]>
    ) {
        foodGateway.getFoodEntries { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let response):
                let parsedResult = self.parseFoodEntriesResponse(response.result)
                let filteredEntries = self.filterFoodEntries(
                    parsedResult,
                    fromDate: fromDate,
                    toDate: toDate
                )
                
                completion(.success(filteredEntries))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func removeFoodEntry(id: String, completion: @escaping EmptyNetworkHandler) {
        foodGateway.removeFoodEntry(id: id, completion: completion)
    }
    
    func getAdminStats(completion: @escaping NetworkHandler<FoodAdminStatsResponse>) {
        foodGateway.getFoodEntries { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let response):
                let entriesCountResponse = self.parseNumberOfAddedEntries(response.result)
                let averageCaloriesResponse = self.parseUsersAverageCalories(response.result)
                
                let statsResponse = FoodAdminStatsResponse(
                    entriesCount: entriesCountResponse,
                    usersAverageCalories: averageCaloriesResponse
                )
                completion(.success(statsResponse))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    private func parseNumberOfAddedEntries(
        _ data: [FoodEntryResponse]
    ) -> FoodEntriesCountStatResponse {
        let offsetInterval: TimeInterval = -86400 * 7
        let thisWeekDate = Date().addingTimeInterval(offsetInterval)
        let weekAgoDate = thisWeekDate.addingTimeInterval(offsetInterval)
        var numberOfEntriesThisWeek = 0
        var numberOfEntriesWeekAgo = 0
        for entry in data {
            if entry.createdAt >= thisWeekDate {
                numberOfEntriesThisWeek += 1
            } else if entry.createdAt >= weekAgoDate {
                numberOfEntriesWeekAgo += 1
            }
        }
        
        return FoodEntriesCountStatResponse(
            thisWeekEntriesCount: numberOfEntriesThisWeek,
            lastWeekEntriesCount: numberOfEntriesWeekAgo
        )
    }
    
    private func parseUsersAverageCalories(
        _ data: [FoodEntryResponse]
    ) -> [UserAverageCaloriesResponse] {
        let minDate = Date().addingTimeInterval(-86400 * 7)
        var usersEntries: [String: [FoodEntryResponse]] = [:]
        for entry in data {
            guard entry.createdAt >= minDate else { continue }
            
            let userId = entry.creatorId
            if (usersEntries[userId] == nil) {
                usersEntries[userId] = []
            }
            usersEntries[userId]?.append(entry)
        }
        
        let result: [UserAverageCaloriesResponse] = usersEntries.map { _, entries in
            let totalCalories = entries.reduce(0, { $0 + $1.calories })
            let count = entries.count
            let average = Int(Float(totalCalories) / Float(count))
            return UserAverageCaloriesResponse(
                username: entries[0].creatorName,
                lastWeekAverageCalories: average
            )
        }
        
        return result
    }
    
    private func filterFoodEntries(
        _ data: [FoodDayEntryData],
        fromDate: Date? = nil,
        toDate: Date? = nil
    ) -> [FoodDayEntryData] {
        var minDate: Date?
        var maxDate: Date?
        
        if let fromDate = fromDate {
            // Round the time to the floor
            var components = Calendar.current.dateComponents([.day, .year, .month], from: fromDate)
            components.hour = 0
            components.minute = 0
            components.second = 0
            if let date = Calendar.current.date(from: components) {
                minDate = date
            }
        }
        
        if let toDate = toDate {
            // Round the time to the ceiling
            var components = Calendar.current.dateComponents([.day, .year, .month], from: toDate)
            components.hour = 23
            components.minute = 59
            components.second = 59
            if let date = Calendar.current.date(from: components) {
                maxDate = date
            }
        }
        
        return data.filter { entry in
            if let minDate = minDate, entry.date < minDate {
                return false
            }
            if let maxDate = maxDate, entry.date > maxDate {
                return false
            }
            return true
        }
    }
    
    private func parseFoodEntriesResponse(
        _ data: [FoodEntryResponse]
    ) -> [FoodDayEntryData] {
        var monthSpendings: [Date: Double] = [:]
        for entry in data {
            let monthComponents = Calendar.current.dateComponents([.year, .month], from: entry.createdAt)
            guard let date = Calendar.current.date(from: monthComponents) else { continue }
            
            monthSpendings[date] = (monthSpendings[date] ?? 0.0) + (entry.price ?? 0.0)
        }
        
        var dayGroups: [Date: [FoodEntryData]] = [:]
        for entry in data {
            let dayComponents = Calendar.current.dateComponents([.day, .year, .month], from: entry.createdAt)
            guard let date = Calendar.current.date(from: dayComponents) else { continue }
            
            let entryData = FoodEntryData(from: entry)
            if dayGroups[date] == nil {
                dayGroups[date] = []
            }
            dayGroups[date]?.append(entryData)
        }
        

        let result: [FoodDayEntryData] = dayGroups.map {
            let monthComponents = Calendar.current.dateComponents([.year, .month], from: $0.key)
            let monthDate = Calendar.current.date(from: monthComponents)
            var monthlyMoneySpent: Double = 0.0
            if let monthDate = monthDate {
                monthlyMoneySpent = monthSpendings[monthDate] ?? 0.0
            }
            
            let dayEntryData = FoodDayEntryData(
                date: $0.key,
                entries: $0.value,
                monthlyMoneySpent: monthlyMoneySpent
            )
            return dayEntryData
        }.sorted(by: {
            $0.date > $1.date
        })
        
        return result
    }
}
