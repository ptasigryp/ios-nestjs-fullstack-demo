//
//  FoodModel.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 30/01/2022.
//

import Foundation

protocol FoodModel {
    
    func getFoodEntries(
        fromDate: Date?,
        toDate: Date?,
        completion: @escaping NetworkHandler<[FoodDayEntryData]>
    )
    
    func addFoodEntry(
        date: Date,
        name: String,
        calories: Int,
        price: Double?,
        completion: @escaping EmptyNetworkHandler
    )
    
    func updateFoodEntry(
        id: String,
        date: Date,
        name: String,
        calories: Int,
        price: Double?,
        completion: @escaping EmptyNetworkHandler
    )
    
    func removeFoodEntry(
        id: String,
        completion: @escaping EmptyNetworkHandler
    )
    
    func getAdminStats(completion: @escaping NetworkHandler<FoodAdminStatsResponse>)
}
