//
//  MockAccountModel.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import Foundation

final class MockAccountModel: AccountModel {
    
    private enum Constants {
        
        static let adminUsername = "admin"
    }
    
    private let userState: UserStateProtocol
    
    init(userState: UserStateProtocol = UserState.shared) {
        self.userState = userState
    }
    
    func logout() {
        userState.clear()
    }
    
    func login(
        username: String,
        password: String,
        completion: @escaping EmptyNetworkHandler
    ) {
        let response = AccountDetailsResponse(
            username: username,
            isAdmin: username == Constants.adminUsername
        )
        userState.update(with: response)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            completion(.success(()))
        }
    }
}
