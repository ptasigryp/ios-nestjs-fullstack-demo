//
//  AccountModel.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import Foundation

protocol AccountModel {

    func login(
        username: String,
        password: String,
        completion: @escaping EmptyNetworkHandler
    )
    
    func logout()
}
