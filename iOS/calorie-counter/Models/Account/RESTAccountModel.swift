//
//  RestAccountModel.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import Foundation

final class RESTAccountModel: AccountModel {
    
    private let userState: UserStateProtocol
    private let authGateway: AuthGatewayClient
    private let accountGateway: AccountGatewayClient
    
    init(
        userState: UserStateProtocol = UserState.shared,
        authGateway: AuthGatewayClient = GatewayClient.shared,
        accountGateway: AccountGatewayClient = GatewayClient.shared
    ) {
        self.userState = userState
        self.authGateway = authGateway
        self.accountGateway = accountGateway
    }
    
    func login(
        username: String,
        password: String,
        completion: @escaping EmptyNetworkHandler
    ) {
        authGateway.login(
            username: username,
            password: password
        ) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let loginDetails):
                let token = loginDetails.token
                self.authGateway.updateAccessToken(token)
                self.loadAccountDetails(completion: completion)
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func logout() {
        userState.clear()
        authGateway.clearAccessToken()
    }
    
    private func loadAccountDetails(completion: @escaping EmptyNetworkHandler) {
        accountGateway.getAccountDetails { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let response):
                self.userState.update(with: response)
                completion(.success(()))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
