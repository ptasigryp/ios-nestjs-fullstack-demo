//
//  Coordinator.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import UIKit

enum CoordinatorPresentationStyle {
    
    case push
    case modal
    case root
}

protocol Coordinator: UINavigationControllerDelegate, UIAdaptivePresentationControllerDelegate {
    
    var supercoordinator: Coordinator? { get set }
    var subcoordinators: [Coordinator] { get set }
    var presentationStyle: CoordinatorPresentationStyle { get }
    var viewController: UIViewController { get }
    var navigationController: UINavigationController? { get }

    func pop(coordinator: Coordinator)
    func removeFromSupercoordinator()
    func findCoordinator(forViewController viewController: UIViewController) -> Coordinator?
    func subcoordinator<T>(ofType coordinatorType: T.Type) -> T? where T: Coordinator

    func insert(coordinator: Coordinator?)
    func removeSubcoordinators()
    func start(coordinator: Coordinator, animated: Bool)
    func dismiss(animated: Bool, completion: (() -> Void)?)
}

class BaseCoordinator: NSObject, Coordinator {

    weak var supercoordinator: Coordinator?
    var subcoordinators: [Coordinator] = []
    let presentationStyle: CoordinatorPresentationStyle
    let viewController: UIViewController

    var navigationController: UINavigationController? {
        viewController as? UINavigationController ??
            viewController.navigationController ??
            supercoordinator?.viewController.navigationController
    }

    init(
        viewController: UIViewController,
        presentationStyle: CoordinatorPresentationStyle
    ) {
        self.viewController = viewController
        self.presentationStyle = presentationStyle
        super.init()
    }

    func pop(coordinator: Coordinator) {
        guard let index = subcoordinators.firstIndex(where: { $0 === coordinator }) else { return }
        subcoordinators.remove(at: index)
        coordinator.navigationController?.delegate = self
    }

    func removeFromSupercoordinator() {
        supercoordinator?.pop(coordinator: self)
    }

    func findCoordinator(forViewController viewController: UIViewController) -> Coordinator? {
        weak var supercoordinator: Coordinator? = self.supercoordinator
        while let coordinator = supercoordinator {
            if viewController === coordinator.viewController {
                return coordinator
            }
            supercoordinator = coordinator.supercoordinator
        }
        return nil
    }

    func subcoordinator<T>(ofType coordinatorType: T.Type) -> T? where T: Coordinator {
        subcoordinators.first { $0 is T } as? T
    }

    func insert(coordinator: Coordinator? = nil) {
        if let coordinator = coordinator {
            coordinator.supercoordinator = self
            subcoordinators.append(coordinator)
        }
        coordinator?.navigationController?.delegate = coordinator
    }

    func removeSubcoordinators() {
        subcoordinators.forEach {
            $0.removeSubcoordinators()
        }
        subcoordinators.removeAll()
    }

    func start(coordinator: Coordinator, animated: Bool = true) {
        switch coordinator.presentationStyle {
        case .modal:
            let navigationController = UINavigationController(rootViewController: coordinator.viewController)
            navigationController.presentationController?.delegate = coordinator
            viewController.present(navigationController, animated: animated, completion: nil)
        case .push:
            navigationController?.pushViewController(coordinator.viewController, animated: animated)
        case .root:
            navigationController?.delegate = coordinator
            navigationController?.setViewControllers([coordinator.viewController], animated: animated)
            removeSubcoordinators()
        }
        insert(coordinator: coordinator)
    }

    func dismiss(animated: Bool = true, completion: (() -> Void)? = nil) {

        let finalize = { [weak self] in
            self?.subcoordinators.removeAll()
            completion?()
            self?.removeFromSupercoordinator()
        }

        switch presentationStyle {
        case .push:
            navigationController?.popViewController(animated: true)
            finalize()
        case .modal:
            navigationController?.dismiss(animated: animated, completion: {
                finalize()
            })
        case .root:
            finalize()
        }
    }

    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        controller.presentedViewController.modalPresentationStyle
    }

    func navigationController(
        _ navigationController: UINavigationController,
        didShow viewController: UIViewController, animated: Bool
    ) {
        guard let coordinator = findCoordinator(forViewController: viewController) else { return }
        
        coordinator.subcoordinators.removeAll()
        navigationController.delegate = coordinator
    }

    func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
        subcoordinators.removeAll()
        removeFromSupercoordinator()
    }
}
