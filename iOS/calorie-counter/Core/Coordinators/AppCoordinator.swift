//
//  AppCoordinator.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import UIKit

final class AppCoordinator {
    
    private enum Constants {
        
        static let startTransitionTime: TimeInterval = 0.3
        static let mainTintColor: UIColor = .systemGreen
    }
    
    // MARK: - Properties
    
    private let window: UIWindow
    private var subcoordinator: Coordinator?
    private let notificationCenter: NotificationCenter
    
    init(
        window: UIWindow,
        notificationCenter: NotificationCenter = .default
    ) {
        self.window = window
        self.notificationCenter = notificationCenter
        
        window.tintColor = Constants.mainTintColor
        setupObservers()
    }
    
    func start() {
//        Model.setMode(.mock) // ONLY FOR DEMO PURPOSE
        
        // TODO: - Add user already logged in logic if needed
        startLoginCoordinator(animated: true)
    }
    
    // MARK: - Methods
    
    private func setupObservers() {
        notificationCenter.addObserver(
            self,
            selector: #selector(logoutNotificationReceived),
            name: .Logout,
            object: nil
        )
    }
    
    @objc private func logoutNotificationReceived() {
        startLoginCoordinator(animated: true)
    }
    
    // MARK: - Flow management
    
    private func startLoginCoordinator(animated: Bool) {
        let coordinator = LoginCoordinator()
        coordinator.delegate = self
        start(coordinator: coordinator, animated: animated)
    }
    
    private func startFoodEntriesCoordinator(animated: Bool) {
        let coordinator = FoodEntriesCoordinator()
        start(coordinator: coordinator, animated: animated)
    }
    
    private func start(coordinator: Coordinator, animated: Bool) {
        window.rootViewController = coordinator.viewController
        window.makeKeyAndVisible()
        subcoordinator = coordinator
        if animated {
            UIView.transition(
                with: window,
                duration: 0.3,
                options: .transitionCrossDissolve,
                animations: nil,
                completion: nil
            )
        }
    }
}

extension AppCoordinator: LoginCoordinatorDelegate {
    
    func loginCoordinatorDidSuccessfullyLogin(_ sender: LoginCoordinator) {
        startFoodEntriesCoordinator(animated: true)
    }
}

