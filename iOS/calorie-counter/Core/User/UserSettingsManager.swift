//
//  UserSettings.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import Foundation

protocol UserSettingsManagerProtocol: AnyObject {
    
    var caloriesLimit: Int { get set }
}

final class UserSettingsManager: UserSettingsManagerProtocol {
    
    private enum Constants {
        
        static let defaultCalorieLimit = 2100
    }
    
    static let shared = UserSettingsManager()
    
    var caloriesLimit: Int {
        get {
            let value = storageManager.int(for: .calorieLimit)
            return value > 0 ? value : Constants.defaultCalorieLimit
        }
        set {
            storageManager.set(newValue, for: .calorieLimit)
        }
    }
    
    private let storageManager: StorageManagerProtocol
    
    init(storageManager: StorageManagerProtocol = StorageManager.shared) {
        self.storageManager = storageManager
    }
}
