//
//  StorageManager.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import Foundation

enum StorageKey: String {
    
    case calorieLimit
}

protocol StorageManagerProtocol: AnyObject {
    
    func set(_ value: Int, for key: StorageKey)
    
    func int(for key: StorageKey) -> Int
}

final class StorageManager: StorageManagerProtocol {
    
    static let shared = StorageManager()
    
    private let defaults: UserDefaults
    
    init(defaults: UserDefaults = .standard) {
        self.defaults = defaults
    }
    
    func set(_ value: Int, for key: StorageKey) {
        defaults.set(value, forKey: key.rawValue)
    }
    
    func int(for key: StorageKey) -> Int {
        defaults.integer(forKey: key.rawValue)
    }
}
