//
//  UserState.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import Foundation

protocol UserStateProtocol: AnyObject {
    
    var username: String? { get }
    var isAdmin: Bool { get }
    
    func update(with accountDetails: AccountDetailsResponse)
    func clear()
}

final class UserState: UserStateProtocol {
    
    static let shared = UserState()
    
    private(set) var username: String?
    private(set) var isAdmin = false
    
    func update(with accountDetails: AccountDetailsResponse) {
        username = accountDetails.username
        isAdmin = accountDetails.isAdmin
    }
    
    func clear() {
        username = nil
        isAdmin = false
    }
}
