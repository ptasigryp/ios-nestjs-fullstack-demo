//
//  ConfigurationLoader.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 30/01/2022.
//

import Foundation

public protocol ConfigurationLoaderProtocol {

    func readConfigurationPlist() throws -> Configuration
}

enum ConfigurationLoaderError: Error {

    case configurationNotFound
    case configurationInitialization
}

public final class ConfigurationLoader: ConfigurationLoaderProtocol {

    // MARK: - Properties

    private let bundleService: BundleServiceProtocol
    private let configurationPlistKeyName: String

    // MARK: - Initialization

    init(
        _ bundleService: BundleServiceProtocol,
        _ configurationPlistKeyName: String = "Configuration"
    ) {
        self.bundleService = bundleService
        self.configurationPlistKeyName = configurationPlistKeyName
    }

    // MARK: - ConfigurationLoader

    /// Returns the `Configuration` for the `plist` provided with the `BundleService`
    public func readConfigurationPlist() throws -> Configuration {
        guard
            let path = bundleService.configurationPath,
            let plist = NSDictionary(contentsOfFile: path) as? [String: Any],
            let currentConfiguration = plist[configurationPlistKeyName] as? String
        else {
            throw ConfigurationLoaderError.configurationNotFound
        }

        guard let configuration = Configuration(rawValue: currentConfiguration) else {
            throw ConfigurationLoaderError.configurationInitialization
        }

        return configuration
    }
}
