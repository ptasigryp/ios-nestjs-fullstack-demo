//
//  BundleService.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 30/01/2022.
//

import Foundation

public protocol BundleServiceProtocol {

    var bundleIdentifier: String? { get }
    var configurationPath: String? { get }
}

// NOTE: This approach will allow us to mock Bundle
public final class BundleService: BundleServiceProtocol {

    enum Constants {

        static let configurationPlistFileName = "Info"
        static let plistFileExtension = "plist"
    }

    // MARK: - Properties

    public let bundle: Bundle?

    /// - Returns: Bundle identifier
    public var bundleIdentifier: String? {
        bundle?.bundleIdentifier
    }

    /// - Returns: The Path to `Info.plist` file
    public var configurationPath: String? {
        bundle?.path(
            forResource: Constants.configurationPlistFileName,
            ofType: Constants.plistFileExtension
        )
    }

    // MARK: - Initialization

    public init(bundle: Bundle?) {
        self.bundle = bundle
    }
}
