//
//  Configuration.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 30/01/2022.
//

import Foundation
import UIKit

public enum Configuration: String {

    // MARK: - configurations

    case debugDevelopment = "Debug"
    case releaseDevelopment = "Release.Dev"

    case debugProduction = "Debug.Prod"
    case releaseProduction = "Release.Prod"

    public static var bundleService: BundleServiceProtocol = {
        class EmptyClass {}
        let bundle = Bundle(for: EmptyClass.self)
        return BundleService(bundle: bundle)
    }()

    public static func loadConfiguration(
        using configurationLoader: ConfigurationLoaderProtocol
    ) -> Configuration {
        do {
            return try configurationLoader.readConfigurationPlist()
        } catch {
            fatalError("Couldn't load app configuration")
        }
    }

    public static let current: Configuration = {
        let configurationLoader = ConfigurationLoader(bundleService)
        return loadConfiguration(using: configurationLoader)
    }()
}

// MARK: - Gateway

extension Configuration {

    enum Gateway {

        static var developmentApiUrl = "http://localhost:8000"
        static var productionApiUrl = "http://localhost:8000" // TODO: - Update to production API
    }

    public var apiURL: String {
        switch self {
        case .debugDevelopment, .releaseDevelopment:
            return Configuration.Gateway.developmentApiUrl
        case .debugProduction, .releaseProduction:
            return Configuration.Gateway.productionApiUrl
        }
    }
}
