//
//  AdminPanelCoordinator.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 01/02/2022.
//

import Foundation

final class AdminPanelCoordinator: BaseCoordinator {
    
    init(presentationStyle: CoordinatorPresentationStyle) {
        let viewModel = AdminPanelViewModel(foodModel: Model.factory.foodModel)
        let viewController = AdminPanelViewController(viewModel: viewModel)
        super.init(viewController: viewController, presentationStyle: presentationStyle)
    }
}
