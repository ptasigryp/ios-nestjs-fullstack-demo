//
//  AdminPanelViewModel.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 01/02/2022.
//

import Foundation

protocol AdminPanelViewModelDelegate: ViewModelDelegateProtocol {
    
    func reloadSections(_ sections: IndexSet)
}

final class AdminPanelViewModel {
    
    struct Section {
        
        let name: String
        var items: [String]
    }
    
    // MARK: - Properties
    
    weak var delegate: AdminPanelViewModelDelegate?
    
    private var sections: [Section] = []
    private let foodModel: FoodModel
    
    // MARK: - Lifecycle
    
    init(foodModel: FoodModel) {
        self.foodModel = foodModel
        initializeSections()
    }
    
    func didLoad() {
        loadEntries()
    }
    
    // MARK: - Data loading & managing
    
    private func initializeSections() {
        sections = [
            Section(
                name: localized("adminPanel_sectionAddedEntriesTitle"),
                items: [
                    localized("adminPanel_entriesThisWeek") + "n/a",
                    localized("adminPanel_entriesLastWeek") + "n/a"
                ]
            ),
            Section(
                name: localized("adminPanel_sectionAverageCaloriesTitle"),
                items: []
            )
        ]
    }
    
    private func loadEntries() {
        foodModel.getAdminStats { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let response):
                self.updateSection(with: response.entriesCount)
                self.updateSection(with: response.usersAverageCalories)
            case .failure:
                let presentation = AlertPresentation.tryAgainAlertPresentation { [weak self] in
                    self?.loadEntries()
                }
                self.delegate?.showAlert(presentation: presentation)
            }
        }
    }
    
    private func updateSection(with response: FoodEntriesCountStatResponse) {
        let thisWeekText = localized("adminPanel_entriesThisWeek") + "\(response.thisWeekEntriesCount)"
        let lastWeekText = localized("adminPanel_entriesLastWeek") + "\(response.lastWeekEntriesCount)"
        sections[0].items = [
            thisWeekText,
            lastWeekText
        ]
        self.delegate?.reloadSections(IndexSet(integer: 0))
    }
    
    private func updateSection(with response: [UserAverageCaloriesResponse]) {
        sections[1].items = response.map {
            "\($0.username): \($0.lastWeekAverageCalories) kcal"
        }
        self.delegate?.reloadSections(IndexSet(integer: 1))
    }
    
    // MARK: - TableView
    
    func numberOfSections() -> Int {
        sections.count
    }
    
    func numberOfEntriesInSection(_ section: Int) -> Int {
        sections[section].items.count
    }
    
    func titleForCell(at indexPath: IndexPath) -> String {
        sections[indexPath.section].items[indexPath.row]
    }
    
    func titleForHeader(in section: Int) -> String {
        sections[section].name
    }
}
