//
//  AdminPanelViewController.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 01/02/2022.
//

import UIKit

final class AdminPanelViewController: UIViewController {
    
    private enum Constant {
        
        static let cellIdentifier = "Cell"
    }
    
    // MARK: - Properties
    
    @IBOutlet private var tableView: UITableView!
    
    private let viewModel: AdminPanelViewModel
    
    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.didLoad()
        setupViews()
        configureTableView()
    }
    
    init(viewModel: AdminPanelViewModel) {
        self.viewModel = viewModel
        super.init(nibName: String(describing: Self.self), bundle: Bundle(for: Self.self))
        viewModel.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - UI Setup
    
    private func setupViews() {
        title = localized("adminPanel_title")
    }
    
    private func configureTableView() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: Constant.cellIdentifier)
        tableView.dataSource = self
    }
}

extension AdminPanelViewController: AdminPanelViewModelDelegate {
    
    func reloadSections(_ sections: IndexSet) {
        tableView.reloadSections(sections, with: .automatic)
    }
}

extension AdminPanelViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfEntriesInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constant.cellIdentifier, for: indexPath)
        let title = viewModel.titleForCell(at: indexPath)
        var content = cell.defaultContentConfiguration()
        content.text = title
        cell.contentConfiguration = content
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        viewModel.titleForHeader(in: section)
    }
}
