//
//  LoginCoordinator.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import Foundation

protocol LoginCoordinatorDelegate: AnyObject {
    
    func loginCoordinatorDidSuccessfullyLogin(_ sender: LoginCoordinator)
}

final class LoginCoordinator: BaseCoordinator {
    
    weak var delegate: LoginCoordinatorDelegate?

    init() {
        let viewModel = LoginViewModel(accountModel: Model.factory.accountModel)
        let viewController = LoginViewController(viewModel: viewModel)
        super.init(viewController: viewController, presentationStyle: .root)
        
        viewModel.coordinatorDelegate = self
    }
}

extension LoginCoordinator: LoginViewModelCoordinatorDelegate {
    
    func loginViewModelDidSuccessfullyLogin(_ sender: LoginViewModel) {
        delegate?.loginCoordinatorDidSuccessfullyLogin(self)
    }
}
