//
//  LoginViewModel.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import Foundation

protocol LoginViewModelDelegate: ViewModelDelegateProtocol {}

protocol LoginViewModelCoordinatorDelegate: AnyObject {
    
    func loginViewModelDidSuccessfullyLogin(_ sender: LoginViewModel)
}

final class LoginViewModel {
    
    // MARK: - Properties
    
    weak var delegate: LoginViewModelDelegate?
    weak var coordinatorDelegate: LoginViewModelCoordinatorDelegate?

    private let accountModel: AccountModel
    
    // MARK: - Init
    
    init(accountModel: AccountModel) {
        self.accountModel = accountModel
    }
    
    // MARK: - Methods
    
    func login(username: String, password: String) {
        guard
            isUsernameValidFormat(username),
            isPasswordValidFormat(password)
        else {
            displayCredentialsInvalidAlert()
            return
        }
        
        delegate?.setLoadingState(loading: true)
        accountModel.login(username: username, password: password) { [weak self] result in
            guard let self = self else { return }
            self.delegate?.setLoadingState(loading: false)
            switch result {
            case .success:
                self.coordinatorDelegate?.loginViewModelDidSuccessfullyLogin(self)
            case .failure:
                self.displayCredentialsInvalidAlert()
            }
        }
    }
    
    private func displayCredentialsInvalidAlert() {
        let presentation = AlertPresentation.infoAlertPresentation(
            title: localized("login_invalidCredentialsTitle"),
            message: localized("login_invalidCredentialsMessage")
        )
        delegate?.showAlert(presentation: presentation)
    }
    
    private func isUsernameValidFormat(_ username: String) -> Bool {
        (2..<32).contains(username.count)
    }
    
    private func isPasswordValidFormat(_ password: String) -> Bool {
        (6..<32).contains(password.count)
    }
}
