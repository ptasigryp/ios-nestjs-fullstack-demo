//
//  FoodEntriesViewModel.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import Foundation

protocol FoodEntriesViewModelDelegate: ViewModelDelegateProtocol {
    
    func reloadFoodEntries()
}

protocol FoodEntriesViewModelCoordinatorDelegate: AnyObject {
    
    func foodEntriesViewModelOpenAdminPanel(_ sender: FoodEntriesViewModel)
    func foodEntriesViewModelOpenProfile(_ sender: FoodEntriesViewModel)
    func foodEntriesViewModelOpenAddNewEntry(_ sender: FoodEntriesViewModel)
    func foodEntriesViewModelOpenEditEntry(
        _ sender: FoodEntriesViewModel,
        entry: FoodEntryData
    )
}

final class FoodEntriesViewModel {
    
    private enum Constant {
        
        static let monthlySpendingLimit: Double = 1000.0
        static var monthlySpendingLimitText = String(format: "%.2f", Self.monthlySpendingLimit)
    }
    
    // MARK: - Properties
    
    weak var delegate: FoodEntriesViewModelDelegate?
    weak var coordinatorDelegate: FoodEntriesViewModelCoordinatorDelegate?
    
    var hasAccessToAdminPanel: Bool {
        userState.isAdmin
    }
    
    private var sections: [FoodDayEntryData] = []
    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        return formatter
    }()
    private let timeFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.timeStyle = .short
        return formatter
    }()
    private(set) var minDate = Date()
    private(set) var maxDate = Date()
    
    private let userState: UserStateProtocol
    private let userSettingsManager: UserSettingsManagerProtocol
    private let foodModel: FoodModel
    
    // MARK: - Lifecycle
    
    init(
        userState: UserStateProtocol = UserState.shared,
        userSettingsManager: UserSettingsManagerProtocol = UserSettingsManager.shared,
        foodModel: FoodModel
    ) {
        self.userState = userState
        self.userSettingsManager = userSettingsManager
        self.foodModel = foodModel
        resetDateRange(shouldReloadEntries: false)
    }
    
    func didLoad() {
        loadEntries()
    }
    
    func reloadEntries() {
        loadEntries()
    }
    
    // MARK: - Methods
    
    func resetDateRange(shouldReloadEntries: Bool = true) {
        minDate = Date(timeIntervalSince1970: 1610233200) // 01-01-2021
        maxDate = Date()
        
        guard shouldReloadEntries else { return }
        reloadEntries()
    }
    
    func applyDateRange(minDate: Date, maxDate: Date) {
        guard maxDate > minDate else { return }
        
        self.minDate = minDate
        self.maxDate = maxDate
        reloadEntries()
    }
    
    private func displayEntryOptionsSheet(for entry: FoodEntryData) {
        let editButton = AlertButtonPresentation(title: "Edit") { [weak self] in
            guard let self = self else { return }
            self.coordinatorDelegate?.foodEntriesViewModelOpenEditEntry(self, entry: entry)
        }
        let removeButton = AlertButtonPresentation(title: "Remove") { [weak self] in
            self?.removeEntry(entry)
        }
        let buttons = [
            editButton,
            removeButton,
            .cancelButton
        ]
        let optionsAlert = AlertPresentation.actionSheetPresentation(
            title: "Select option",
            message: "",
            buttons: buttons
        )
        
        delegate?.showAlert(presentation: optionsAlert)
    }
    
    // MARK: - Actions
    
    func addButtonTapped() {
        coordinatorDelegate?.foodEntriesViewModelOpenAddNewEntry(self)
    }
    
    func profileButtonTapped() {
        coordinatorDelegate?.foodEntriesViewModelOpenProfile(self)
    }
    
    func adminPanelButtonTapped() {
        coordinatorDelegate?.foodEntriesViewModelOpenAdminPanel(self)
    }
    
    func didSelectCell(at indexPath: IndexPath) {
        guard userState.isAdmin else { return }
        
        displayEntryOptionsSheet(for: entry(for: indexPath))
    }
    
    // MARK: - Data loading & managing
    
    private func removeEntry(_ entry: FoodEntryData) {
        delegate?.setLoadingState(loading: true)
        foodModel.removeFoodEntry(id: entry.id) { [weak self] result in
            guard let self = self else { return }
            self.delegate?.setLoadingState(loading: false)
            switch result {
            case .success:
                self.reloadEntries()
            case .failure:
                let presentation = AlertPresentation.tryAgainAlertPresentation { [weak self] in
                    self?.removeEntry(entry)
                }
                self.delegate?.showAlert(presentation: presentation)
            }
        }
    }
    
    private func loadEntries() {
        delegate?.setLoadingState(loading: true)
        foodModel.getFoodEntries(fromDate: minDate, toDate: maxDate) { [weak self] result in
            guard let self = self else { return }
            self.delegate?.setLoadingState(loading: false)
            switch result {
            case .success(let dayEntries):
                self.sections = dayEntries
                self.delegate?.reloadFoodEntries()
            case .failure:
                let presentation = AlertPresentation.tryAgainAlertPresentation(
                    title: localized("general_error"),
                    message: localized("foodEntries_loadingFailedMessage"),
                    tryAgainAction: { [weak self] in
                        self?.loadEntries()
                    }
                )
                self.delegate?.showAlert(presentation: presentation)
            }
        }
    }
    
    private func dateString(for date: Date) -> String {
        dateFormatter.string(from: date)
    }
    
    private func timeString(for date: Date) -> String {
        timeFormatter.string(from: date)
    }
    
    // MARK: - TableView
    
    private func entry(for indexPath: IndexPath) -> FoodEntryData {
        sections[indexPath.section].entries[indexPath.row]
    }
    
    func numberOfSections() -> Int {
        sections.count
    }
    
    func numberOfEntriesInSection(_ section: Int) -> Int {
        sections[section].entries.count
    }
    
    func titleForCell(at indexPath: IndexPath) -> String {
        let entry = entry(for: indexPath)
        let productName = entry.productName
        let kcal = entry.kcal
        var title = "\(productName) (\(kcal) kcal)"
        if let priceText = entry.priceString {
            title += " $\(priceText)"
        }
        if userState.isAdmin {
            title += " [\(entry.creatorName)]"
        }
        return title
    }
    
    func subtitleForCell(at indexPath: IndexPath) -> String {
        let entry = entry(for: indexPath)
        let date = entry.date
        let formattedTime = timeString(for: date)
        return formattedTime
    }
    
    func titleForHeader(in section: Int) -> String {
        let dayEntry = sections[section]
        let date = dayEntry.date
        let totalCalories = dayEntry.totalCalories
        let totalSpendings = dayEntry.monthlyMoneySpent
        let caloriesLimit = userSettingsManager.caloriesLimit
        var caloriesLimitText = "\n\(totalCalories) / \(caloriesLimit) kcal"
        if totalCalories >= caloriesLimit {
            caloriesLimitText += " - \(localized("general_limitReached"))!"
        }
        var budgetText = "\n$\(dayEntry.monthlyMoneySpentString) / $\(Constant.monthlySpendingLimitText)"
        if totalSpendings > Constant.monthlySpendingLimit {
            budgetText += " - \(localized("general_limitReached"))!"
        }
        return "\(dateString(for: date))\(caloriesLimitText)\(budgetText)"
    }
}
