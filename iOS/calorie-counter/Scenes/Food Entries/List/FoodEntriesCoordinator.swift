//
//  FoodEntriesCoordinator.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import Foundation
import UIKit

final class FoodEntriesCoordinator: BaseCoordinator {
    
    private weak var foodEntriesViewModel: FoodEntriesViewModel?
    
    init() {
        let viewModel = FoodEntriesViewModel(foodModel: Model.factory.foodModel)
        let viewController = FoodEntriesViewController(viewModel: viewModel)
        foodEntriesViewModel = viewModel
        
        super.init(viewController: UINavigationController(rootViewController: viewController), presentationStyle: .root)
        viewModel.coordinatorDelegate = self
    }
}

extension FoodEntriesCoordinator: FoodEntriesViewModelCoordinatorDelegate {
    
    func foodEntriesViewModelOpenAdminPanel(_ sender: FoodEntriesViewModel) {
        let coordinator = AdminPanelCoordinator(presentationStyle: .modal)
        start(coordinator: coordinator)
    }
    
    func foodEntriesViewModelOpenProfile(_ sender: FoodEntriesViewModel) {
        let coordinator = ProfileCoordinator(presentationStyle: .modal)
        coordinator.delegate = self
        start(coordinator: coordinator)
    }
    
    func foodEntriesViewModelOpenAddNewEntry(_ sender: FoodEntriesViewModel) {
        startCreateEditEntryCoordinator(mode: .create)
    }
    
    func foodEntriesViewModelOpenEditEntry(
        _ sender: FoodEntriesViewModel,
        entry: FoodEntryData
    ) {
        startCreateEditEntryCoordinator(mode: .edit(entry))
    }
    
    private func startCreateEditEntryCoordinator(mode: FoodCreateEditEntryViewModel.Mode) {
        let coordinator = FoodCreateEditEntryCoordinator(
            mode: mode,
            presentationStyle: .modal
        )
        coordinator.delegate = self
        start(coordinator: coordinator)
    }
}

extension FoodEntriesCoordinator: FoodCreateEditEntryCoordinatorDelegate {
    
    func foodNewEntryCoordinatorDidCreateEditEntry(_ sender: FoodCreateEditEntryCoordinator) {
        foodEntriesViewModel?.reloadEntries()
    }
}

extension FoodEntriesCoordinator: ProfileCoordinatorDelegate {
    
    func profileCoordinatorDidSaveSettings(_ sender: ProfileCoordinator) {
        foodEntriesViewModel?.reloadEntries()
    }
}
