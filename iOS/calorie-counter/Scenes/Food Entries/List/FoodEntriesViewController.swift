//
//  FoodEntriesViewController.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import UIKit

final class FoodEntriesViewController: UIViewController {
    
    private enum Constant {
        
        static let cellIdentifier = "Cell"
        static let addButtonCornerRadius: CGFloat = 30.0
        static let topInset: CGFloat = 40.0
        static let headerHeight: CGFloat = 52.0
    }
    
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var addButton: UIButton!
    @IBOutlet private var startDateLabel: UILabel!
    @IBOutlet private var endDateLabel: UILabel!
    @IBOutlet private var startDatePicker: UIDatePicker!
    @IBOutlet private var endDatePicker: UIDatePicker!
    @IBOutlet private var resetFiltersButton: UIButton!
    @IBOutlet private var applyFiltersButton: UIButton!
    
    private let viewModel: FoodEntriesViewModel
    
    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setupObservers()
        configureTableView()
        
        viewModel.didLoad()
    }
    
    init(viewModel: FoodEntriesViewModel) {
        self.viewModel = viewModel
        super.init(nibName: String(describing: Self.self), bundle: Bundle(for: Self.self))
        viewModel.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Actions
    
    @objc private func addButtonTapped() {
        viewModel.addButtonTapped()
    }
    
    @objc private func resetDatesFilterButtonTapped() {
        viewModel.resetDateRange()
        startDatePicker.date = viewModel.minDate
        endDatePicker.date = viewModel.maxDate
    }
    
    @objc private func applyDatesFilterButtonTapped() {
        viewModel.applyDateRange(minDate: startDatePicker.date, maxDate: endDatePicker.date)
    }
    
    @objc private func profileButtonTapped() {
        viewModel.profileButtonTapped()
    }
    
    @objc private func adminPanelButtonTapped() {
        viewModel.adminPanelButtonTapped()
    }
    
    // MARK: - UI Setup
    
    private func setupViews() {
        title = localized("foodEntries_title")
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            image: UIImage(named: "profile"),
            style: .plain,
            target: self,
            action: #selector(profileButtonTapped)
        )
        if viewModel.hasAccessToAdminPanel {
            navigationItem.rightBarButtonItem = UIBarButtonItem(
                title: "Admin",
                style: .plain,
                target: self,
                action: #selector(adminPanelButtonTapped)
            )
        }
        addButton.layer.cornerRadius = Constant.addButtonCornerRadius
        addButton.layer.masksToBounds = true
        startDateLabel.text = localized("foodEntries_filterStartDate")
        startDatePicker.date = viewModel.minDate
        endDateLabel.text = localized("foodEntries_filterEndDate")
        endDatePicker.date = viewModel.maxDate
        resetFiltersButton.setTitle(localized("general_reset"), for: .normal)
        applyFiltersButton.setTitle(localized("general_apply"), for: .normal)
    }
    
    private func setupObservers() {
        addButton.addTarget(self, action: #selector(addButtonTapped), for: .touchUpInside)
        resetFiltersButton.addTarget(self, action: #selector(resetDatesFilterButtonTapped), for: .touchUpInside)
        applyFiltersButton.addTarget(self, action: #selector(applyDatesFilterButtonTapped), for: .touchUpInside)
    }
    
    private func configureTableView() {
        tableView.contentInset.top = Constant.topInset
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: Constant.cellIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
    }
}

extension FoodEntriesViewController: FoodEntriesViewModelDelegate {
    
    func reloadFoodEntries() {
        tableView.reloadData()
    }
}

extension FoodEntriesViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfEntriesInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constant.cellIdentifier, for: indexPath)
        let title = viewModel.titleForCell(at: indexPath)
        let subtitle = viewModel.subtitleForCell(at: indexPath)
        var content = cell.defaultContentConfiguration()
        content.text = title
        content.secondaryText = subtitle
        cell.contentConfiguration = content
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        viewModel.titleForHeader(in: section)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        Constant.headerHeight
    }
}

extension FoodEntriesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelectCell(at: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
