//
//  FoodNewEntryViewController.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 31/01/2022.
//

import UIKit

final class FoodCreateEditEntryViewController: UIViewController, FoodCreateEditEntryViewModelDelegate {
    
    // MARK: - Properties
    
    @IBOutlet private var nameTextField: UITextField!
    @IBOutlet private var caloriesTextField: UITextField!
    @IBOutlet private var priceTextField: UITextField!
    @IBOutlet private var datePicker: UIDatePicker!
    @IBOutlet private var saveButton: UIButton!
    
    private let viewModel: FoodCreateEditEntryViewModel
    
    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setupObservers()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        nameTextField.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        resignFirstResponder()
    }
    
    init(viewModel: FoodCreateEditEntryViewModel) {
        self.viewModel = viewModel
        super.init(nibName: String(describing: Self.self), bundle: Bundle(for: Self.self))
        viewModel.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - UI Setup
    
    private func setupViews() {
        title = localized("addEntry_title")
        nameTextField.placeholder = localized("addEntry_namePlaceholder")
        caloriesTextField.placeholder = localized("addEntry_caloriesPlaceholder")
        priceTextField.placeholder = localized("addEntry_pricePlaceholder")
        datePicker.maximumDate = Date()
        saveButton.setTitle(localized("general_save"), for: .normal)
        
        if case .edit(let entryData) = viewModel.mode {
            nameTextField.text = entryData.productName
            caloriesTextField.text = String(entryData.kcal)
            priceTextField.text = entryData.priceString
            datePicker.date = entryData.date
        }
    }
    
    private func setupObservers() {
        saveButton.addTarget(self, action: #selector(addButtonTapped), for: .touchUpInside)
    }
    
    // MARK: - Actions
    
    @objc private func addButtonTapped() {
        let date = datePicker.date
        let name = nameTextField.text ?? ""
        let calories = caloriesTextField.text ?? ""
        let price = priceTextField.text
        viewModel.save(date: date, name: name, calories: calories, price: price)
    }
}
