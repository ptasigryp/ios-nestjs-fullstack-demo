//
//  FoodNewEntryCoordinator.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 31/01/2022.
//

import Foundation

protocol FoodCreateEditEntryCoordinatorDelegate: AnyObject {
    
    func foodNewEntryCoordinatorDidCreateEditEntry(_ sender: FoodCreateEditEntryCoordinator)
}

final class FoodCreateEditEntryCoordinator: BaseCoordinator {
    
    weak var delegate: FoodCreateEditEntryCoordinatorDelegate?
    
    init(
        mode: FoodCreateEditEntryViewModel.Mode,
        presentationStyle: CoordinatorPresentationStyle
    ) {
        let viewModel = FoodCreateEditEntryViewModel(
            foodModel: Model.factory.foodModel,
            mode: mode
        )
        let viewController = FoodCreateEditEntryViewController(viewModel: viewModel)
        super.init(viewController: viewController, presentationStyle: presentationStyle)
        viewModel.coordinatorDelegate = self
    }
}

extension FoodCreateEditEntryCoordinator: FoodCreateEditEntryViewModelCoordinatorDelegate {
    
    func foodNewEntryViewModelDidCreateEditEntry(_ sender: FoodCreateEditEntryViewModel) {
        delegate?.foodNewEntryCoordinatorDidCreateEditEntry(self)
        dismiss()
    }
}
