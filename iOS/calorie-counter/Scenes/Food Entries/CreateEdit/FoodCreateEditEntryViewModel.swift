//
//  FoodNewEntryViewModel.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 31/01/2022.
//

import Foundation

protocol FoodCreateEditEntryViewModelDelegate: ViewModelDelegateProtocol {}

protocol FoodCreateEditEntryViewModelCoordinatorDelegate: AnyObject {
    
    func foodNewEntryViewModelDidCreateEditEntry(_ sender: FoodCreateEditEntryViewModel)
}

final class FoodCreateEditEntryViewModel {
    
    enum Mode {
        
        case create
        case edit(FoodEntryData)
    }
    
    // MARK: - Properties
    
    weak var delegate: FoodCreateEditEntryViewModelDelegate?
    weak var coordinatorDelegate: FoodCreateEditEntryViewModelCoordinatorDelegate?
    
    private let foodModel: FoodModel
    private(set) var mode: Mode
    
    // MARK: - Lifecycle
    
    init(foodModel: FoodModel, mode: Mode) {
        self.foodModel = foodModel
        self.mode = mode
    }
    
    // MARK: - Methods
    
    func save(date: Date, name: String, calories: String, price: String?) {
        guard
            isNameValidFormat(name),
            let caloriesValue = parseCaloriesString(calories)
        else {
            displayErrorAlert()
            return
        }
        
        let priceValue = parsePriceString(price)
        
        switch mode {
        case .edit(let oldFoodEntry):
            updateEntry(
                id: oldFoodEntry.id,
                date: date,
                name: name,
                calories: caloriesValue,
                price: priceValue
            )
        case .create:
            createEntry(
                date: date,
                name: name,
                calories: caloriesValue,
                price: priceValue
            )
        }
    }
    
    private func updateEntry(id: String, date: Date, name: String, calories: Int, price: Double?) {
        delegate?.setLoadingState(loading: true)
        foodModel.updateFoodEntry(
            id: id,
            date: date,
            name: name,
            calories: calories,
            price: price
        ) { [weak self] result in
            guard let self = self else { return }
            self.delegate?.setLoadingState(loading: false)
            switch result {
            case .success:
                self.coordinatorDelegate?.foodNewEntryViewModelDidCreateEditEntry(self)
            case .failure:
                self.displayErrorAlert()
            }
        }
    }
    
    private func createEntry(date: Date, name: String, calories: Int, price: Double?) {
        delegate?.setLoadingState(loading: true)
        foodModel.addFoodEntry(
            date: date,
            name: name,
            calories: calories,
            price: price
        ) { [weak self] result in
            guard let self = self else { return }
            self.delegate?.setLoadingState(loading: false)
            switch result {
            case .success:
                self.coordinatorDelegate?.foodNewEntryViewModelDidCreateEditEntry(self)
            case .failure:
                self.displayErrorAlert()
            }
        }
    }
        
    private func displayErrorAlert() {
        let presentation = AlertPresentation.infoAlertPresentation(
            title: localized("general_error"),
            message: localized("addEntry_invalidDataMessage")
        )
        delegate?.showAlert(presentation: presentation)
    }
    
    private func isNameValidFormat(_ name: String) -> Bool {
        (2..<80).contains(name.count)
    }
    
    private func parseCaloriesString(_ text: String) -> Int? {
        guard let value = Int(text) else { return nil }
        guard (1...10_000).contains(value) else { return nil}
        return value
    }
    
    private func parsePriceString(_ text: String?) -> Double? {
        guard let text = text, let value = Double(text) else { return nil }
        guard value >= 0 else { return nil }
        return value
    }
}
