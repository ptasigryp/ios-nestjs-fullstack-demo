//
//  ProfileCoordinator.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 31/01/2022.
//

import Foundation

protocol ProfileCoordinatorDelegate: AnyObject {
    
    func profileCoordinatorDidSaveSettings(_ sender: ProfileCoordinator)
}

final class ProfileCoordinator: BaseCoordinator {
    
    weak var delegate: ProfileCoordinatorDelegate?
    
    init(presentationStyle: CoordinatorPresentationStyle) {
        let viewModel = ProfileViewModel(accountModel: Model.factory.accountModel)
        let viewController = ProfileViewController(viewModel: viewModel)
        super.init(viewController: viewController, presentationStyle: presentationStyle)
        viewModel.coordinatorDelegate = self
    }
}

extension ProfileCoordinator: ProfileViewModelCoordinatorDelegate {
    
    func profileViewModelDidSaveSettings(_ sender: ProfileViewModel) {
        delegate?.profileCoordinatorDidSaveSettings(self)
        dismiss()
    }
}
