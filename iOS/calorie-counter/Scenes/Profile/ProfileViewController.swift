//
//  ProfileViewController.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 31/01/2022.
//

import UIKit

final class ProfileViewController: UIViewController, ProfileViewModelDelegate {
    
    private enum Constant {
        
        static let minCalories = 500
        static let maxCalories = 5000
    }
    
    // MARK: - Properties
    
    @IBOutlet private var saveButton: UIButton!
    @IBOutlet private var logoutButton: UIButton!
    @IBOutlet private var caloriesLimitLabel: UILabel!
    @IBOutlet private var caloriesLimitSlider: UISlider!

    private let viewModel: ProfileViewModel
    
    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setupObservers()
    }
    
    init(viewModel: ProfileViewModel) {
        self.viewModel = viewModel
        super.init(nibName: String(describing: Self.self), bundle: Bundle(for: Self.self))
        viewModel.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Actions
    
    @objc private func saveButtonTapped() {
        viewModel.saveButtonTapped()
    }
    
    @objc private func logoutButtonTapped() {
        viewModel.logoutButtonTapped()
    }
    
    @objc private func caloriesLimitSliderValueChanged() {
        let value = Int(caloriesLimitSlider.value)
        viewModel.updateCaloriesLimit(value)
        updateCaloriesLimitText(viewModel.caloriesLimit)
    }
    
    // MARK: - UI Setup
    
    private func setupViews() {
        title = localized("profile_title") + " " + viewModel.username
        saveButton.setTitle(localized("general_save"), for: .normal)
        logoutButton.setTitle(localized("profile_logoutButton"), for: .normal)
        caloriesLimitSlider.minimumValue = Float(Constant.minCalories)
        caloriesLimitSlider.maximumValue = Float(Constant.maxCalories)
        caloriesLimitSlider.value = Float(viewModel.caloriesLimit)
        updateCaloriesLimitText(viewModel.caloriesLimit)
    }
    
    private func setupObservers() {
        saveButton.addTarget(self, action: #selector(saveButtonTapped), for: .touchUpInside)
        logoutButton.addTarget(self, action: #selector(logoutButtonTapped), for: .touchUpInside)
        caloriesLimitSlider.addTarget(self, action: #selector(caloriesLimitSliderValueChanged), for: .valueChanged)
    }
    
    private func updateCaloriesLimitText(_ calories: Int) {
        caloriesLimitLabel.text = localized("profile_caloriesLimitLabel") + "\(calories)"
    }
}
