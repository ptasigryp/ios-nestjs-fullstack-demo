//
//  ProfileViewModel.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 31/01/2022.
//

import Foundation

protocol ProfileViewModelDelegate: ViewModelDelegateProtocol {}

protocol ProfileViewModelCoordinatorDelegate: AnyObject {
    
    func profileViewModelDidSaveSettings(_ sender: ProfileViewModel)
}

final class ProfileViewModel {
    
    // MARK: - Properties
    
    weak var delegate: ProfileViewModelDelegate?
    weak var coordinatorDelegate: ProfileViewModelCoordinatorDelegate?
    
    private let userState: UserStateProtocol
    private let userSettingsManager: UserSettingsManagerProtocol
    private let accountModel: AccountModel
    private let notificationCenter: NotificationCenter
    
    private(set) var caloriesLimit: Int
    var username: String {
        userState.username ?? ""
    }
    
    // MARK: - Lifecycle
    
    init(
        userState: UserStateProtocol = UserState.shared,
        userSettingsManager: UserSettingsManagerProtocol = UserSettingsManager.shared,
        accountModel: AccountModel,
        notificationCenter: NotificationCenter = .default
    ) {
        self.userState = userState
        self.userSettingsManager = userSettingsManager
        self.accountModel = accountModel
        self.notificationCenter = notificationCenter
        self.caloriesLimit = userSettingsManager.caloriesLimit
    }
    
    // MARK: - Actions
    
    func saveButtonTapped() {
        userSettingsManager.caloriesLimit = caloriesLimit
        coordinatorDelegate?.profileViewModelDidSaveSettings(self)
    }
    
    func logoutButtonTapped() {
        accountModel.logout()
        notificationCenter.post(name: .Logout, object: nil)
    }
    
    func updateCaloriesLimit(_ limit: Int) {
        guard limit > 0 else { return }
        let rounded = (limit / 50) * 50
        caloriesLimit = rounded
    }
}
