//
//  GatewayClient+Auth.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import Foundation

protocol AuthGatewayClient {
    
    func updateAccessToken(_ token: String)
    
    func clearAccessToken()
    
    func login(
        username: String,
        password: String,
        completion: @escaping NetworkHandler<LoginDetailsResponse>
    )
}

extension GatewayClient: AuthGatewayClient {
    
    func updateAccessToken(_ token: String) {
        session.accessToken = token
    }
    
    func clearAccessToken() {
        session.accessToken = ""
    }
    
    func login(
        username: String,
        password: String,
        completion: @escaping NetworkHandler<LoginDetailsResponse>
    ) {
        request(
            provider: authProvider,
            target: .login(username: username, password: password),
            responseType: LoginDetailsResponse.self,
            completion: completion
        )
    }
}
