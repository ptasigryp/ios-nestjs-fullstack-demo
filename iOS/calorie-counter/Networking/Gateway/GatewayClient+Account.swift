//
//  GatewayClient+Account.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import Foundation

protocol AccountGatewayClient {
    
    func getAccountDetails(completion: @escaping NetworkHandler<AccountDetailsResponse>)
}

extension GatewayClient: AccountGatewayClient {
    
    func getAccountDetails(completion: @escaping NetworkHandler<AccountDetailsResponse>) {
        request(
            provider: accountProvider,
            target: .info,
            responseType: AccountDetailsResponse.self,
            completion: completion
        )
    }
}
