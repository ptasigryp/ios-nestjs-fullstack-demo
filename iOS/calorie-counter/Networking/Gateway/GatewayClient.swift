//
//  GatewayClient.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import Foundation
import Moya

final class GatewayClient {
    
    static let shared = GatewayClient()
    
    let session: SessionProtocol
    private lazy var authPlugin = AccessTokenPlugin { _ in
        self.session.accessToken
    }
    private(set) lazy var authProvider = MoyaProvider<AuthService>(plugins: [authPlugin])
    private(set) lazy var accountProvider = MoyaProvider<AccountService>(plugins: [authPlugin])
    private(set) lazy var foodProvider = MoyaProvider<FoodService>(plugins: [authPlugin])
    
    init(session: SessionProtocol = Session.shared) {
        self.session = session
    }
    
    @discardableResult
    func request<ResponseType: Decodable, Target: TargetType>(
        provider: MoyaProvider<Target>,
        target: Target,
        responseType: ResponseType.Type,
        completion: @escaping NetworkHandler<ResponseType>
    ) -> Moya.Cancellable {
        func handleMoyaError(_ error: MoyaError) {
            let endpoint = error.response?.response?.url?.absoluteString ?? "Not Found"
            let httpCode = error.response?.statusCode ?? 0
            print(
            """
                Unknown Gateway Error =>
                endpoint: \(endpoint)
                http code: \(httpCode)
                message: \(error.localizedDescription)
            """
            )
            completion(.failure(AppError.gatewayError(error)))
        }
        
        return provider.request(target) { [weak self] result in
            switch result {
            case .success(let response):
                do {
                    let filteredResponse = try response.filterSuccessfulStatusCodes()
                    self?.parseResponse(response: filteredResponse, responseType: responseType, completion: completion)
                }
                catch let error {
                    completion(.failure(error))
                }
                
            case .failure(let moyaError):
                handleMoyaError(moyaError)
            }
        }
    }
    
    private func parseResponse<ResponseType: Decodable>(
        response: Response,
        responseType: ResponseType.Type,
        completion: NetworkHandler<ResponseType>
    ) {
        do {
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .formatted(DateFormatter.iso8601Full)
            let response = try response.map(ResponseType.self, using: decoder)
            completion(.success(response))
        } catch {
            print("Error decoding response. Error: \(error)")
            completion(.failure(AppError.decodingError(error)))
        }
    }
    
    func handleEmptyResponseDefaultResults<T>(
        _ completion: @escaping EmptyNetworkHandler
    ) -> NetworkHandler<T> {
        return { result in
            switch result {
            case .success:
                completion(.success(()))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
