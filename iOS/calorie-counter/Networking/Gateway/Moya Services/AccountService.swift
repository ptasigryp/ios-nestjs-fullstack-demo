//
//  AccountService.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 30/01/2022.
//

import Foundation
import Moya

enum AccountService {
    
    case info
}

extension AccountService: TargetType {
    
    var baseURL: URL {
        guard let url = URL(string: "\(Configuration.current.apiURL)/user") else {
            fatalError("Base url not configured")
        }
        return url
    }
    
    var path: String {
        switch self {
        case .info:
            return "/info"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .info:
            return .get
        }
    }
    
    var sampleData: Data {
        Data()
    }
    
    var task: Task {
        switch self {
        case .info:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        defaultHeaders
    }
}

extension AccountService: AccessTokenAuthorizable {
    
    var authorizationType: AuthorizationType? {
        switch self {
        case .info:
            return .bearer
        }
    }
}
