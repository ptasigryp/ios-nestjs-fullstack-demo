//
//  FoodService.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 31/01/2022.
//

import Foundation
import Moya

enum FoodService {
    
    case getEntries
    case createEntry(parameters: CreateUpdateEntryParameters)
    case updateEntry(id: String, parameters: CreateUpdateEntryParameters)
    case deleteEntry(id: String)
}

extension FoodService: TargetType {
    
    var baseURL: URL {
        guard let url = URL(string: "\(Configuration.current.apiURL)/food-entries") else {
            fatalError("Base url not configured")
        }
        return url
    }
    
    var path: String {
        switch self {
        case .getEntries, .createEntry:
            return "/"
        case .updateEntry(let id, _),
             .deleteEntry(let id):
            return "/\(id)/"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .createEntry:
            return .post
        case .getEntries:
            return .get
        case .deleteEntry:
            return .delete
        case .updateEntry:
            return .patch
        }
    }
    
    var sampleData: Data {
        Data()
    }
    
    var task: Task {
        switch self {
        case .createEntry(let parameters),
             .updateEntry(_, let parameters):
            return .requestCustomJSONEncodable(
                parameters,
                encoder: defaultEncoder
            )
        case .getEntries, .deleteEntry:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        defaultHeaders
    }
}

extension FoodService: AccessTokenAuthorizable {
    
    var authorizationType: AuthorizationType? {
        switch self {
        case .createEntry,
             .getEntries,
             .deleteEntry,
             .updateEntry:
            return .bearer
        }
    }
}
