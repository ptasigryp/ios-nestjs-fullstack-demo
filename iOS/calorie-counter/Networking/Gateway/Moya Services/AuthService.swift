//
//  AuthService.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 30/01/2022.
//

import Foundation
import Moya

enum AuthService {
    
    case login(username: String, password: String)
}

extension AuthService: TargetType {
    
    var baseURL: URL {
        guard let url = URL(string: "\(Configuration.current.apiURL)/auth") else {
            fatalError("Base url not configured")
        }
        return url
    }
    
    var path: String {
        switch self {
        case .login:
            return "/login"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .login:
            return .post
        }
    }
    
    var sampleData: Data {
        Data()
    }
    
    var task: Task {
        switch self {
        case .login(let username, let password):
            return .requestParameters(
                parameters: [
                    "username": username,
                    "password": password
                ],
                encoding: JSONEncoding.default
            )
        }
    }
    
    var headers: [String : String]? {
        defaultHeaders
    }
}

extension AuthService: AccessTokenAuthorizable {
    
    var authorizationType: AuthorizationType? {
        switch self {
        case .login:
            return .none
        }
    }
}
