//
//  GatewayClient+Food.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 31/01/2022.
//

import Foundation

protocol FoodGatewayClient {
    
    func addFoodEntry(
        date: Date,
        name: String,
        calories: Int,
        price: Double?,
        completion: @escaping EmptyNetworkHandler
    )
    func updateFoodEntry(
        id: String,
        date: Date,
        name: String,
        calories: Int,
        price: Double?,
        completion: @escaping EmptyNetworkHandler
    )
    func getFoodEntries(completion: @escaping NetworkHandler<FoodEntriesResponse>)
    func removeFoodEntry(id: String, completion: @escaping EmptyNetworkHandler)
}

extension GatewayClient: FoodGatewayClient {
    
    func addFoodEntry(
        date: Date,
        name: String,
        calories: Int,
        price: Double?,
        completion: @escaping EmptyNetworkHandler
    ) {
        let parameters = CreateUpdateEntryParameters(
            date: date,
            name: name,
            calories: calories,
            price: price
        )
        request(
            provider: foodProvider,
            target: .createEntry(parameters: parameters),
            responseType: EmptyResponse.self,
            completion: handleEmptyResponseDefaultResults(completion)
        )
    }
    
    func updateFoodEntry(
        id: String,
        date: Date,
        name: String,
        calories: Int,
        price: Double?,
        completion: @escaping EmptyNetworkHandler
    ) {
        let parameters = CreateUpdateEntryParameters(
            date: date,
            name: name,
            calories: calories,
            price: price
        )
        request(
            provider: foodProvider,
            target: .updateEntry(id: id, parameters: parameters),
            responseType: EmptyResponse.self,
            completion: handleEmptyResponseDefaultResults(completion)
        )
    }
    
    func getFoodEntries(completion: @escaping NetworkHandler<FoodEntriesResponse>) {
        request(
            provider: foodProvider,
            target: .getEntries,
            responseType: FoodEntriesResponse.self,
            completion: completion
        )
    }
    
    func removeFoodEntry(id: String, completion: @escaping EmptyNetworkHandler) {
        request(
            provider: foodProvider,
            target: .deleteEntry(id: id),
            responseType: EmptyResponse.self,
            completion: handleEmptyResponseDefaultResults(completion)
        )
    }
}
