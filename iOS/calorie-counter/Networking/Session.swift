//
//  Session.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import Foundation

protocol SessionProtocol: AnyObject {
    
    var accessToken: String { get set }
    
    func clear()
}

class Session: SessionProtocol {
    
    static let shared = Session()
    
    var accessToken: String = ""
    
    func clear() {
        accessToken = ""
    }
}
