//
//  EmptyResponse.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import Foundation

struct EmptyResponse: Decodable {

    public init() {}
}
