//
//  AccountDetailsResponse.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import Foundation

struct AccountDetailsResponse: Decodable {

    let username: String
    let isAdmin: Bool
}
