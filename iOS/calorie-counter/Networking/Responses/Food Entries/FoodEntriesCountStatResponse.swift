//
//  FoodEntriesCountStatResponse.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 01/02/2022.
//

import Foundation

struct FoodEntriesCountStatResponse {
    
    let thisWeekEntriesCount: Int
    let lastWeekEntriesCount: Int
}
