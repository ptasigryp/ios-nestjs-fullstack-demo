//
//  UserAverageCaloriesResponse.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 01/02/2022.
//

import Foundation

struct UserAverageCaloriesResponse {
    
    let username: String
    let lastWeekAverageCalories: Int
}
