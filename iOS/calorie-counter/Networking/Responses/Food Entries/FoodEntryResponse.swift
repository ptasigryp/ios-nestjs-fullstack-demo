//
//  FoodEntriesResponse.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 31/01/2022.
//

import Foundation

typealias FoodEntriesResponse = DefaultResponse<[FoodEntryResponse]>

struct FoodEntryResponse: Decodable {
    
    let id: String
    let creatorId: String
    let creatorName: String
    let name: String
    let calories: Int
    let price: Double?
    let createdAt: Date
}
