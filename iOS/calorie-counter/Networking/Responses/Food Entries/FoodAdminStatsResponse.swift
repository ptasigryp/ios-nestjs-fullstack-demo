//
//  FoodAdminStatsResponse.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 01/02/2022.
//

import Foundation

struct FoodAdminStatsResponse {
    
    let entriesCount: FoodEntriesCountStatResponse
    let usersAverageCalories: [UserAverageCaloriesResponse]
}
