//
//  DefaultResponse.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 31/01/2022.
//

import Foundation

struct DefaultResponse<T: Decodable>: Decodable {
    
    let result: T
}
