//
//  CreateUpdateEntryParameters.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 31/01/2022.
//

import Foundation

struct CreateUpdateEntryParameters: Encodable {
    
    let date: Date
    let name: String
    let calories: Int
    let price: Double?
}
