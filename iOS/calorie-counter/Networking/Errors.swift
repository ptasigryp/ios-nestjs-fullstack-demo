//
//  Errors.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import Foundation

enum AppError: Error {
    
    case unknown
    case gatewayError(Error)
    case decodingError(Error?)
}
