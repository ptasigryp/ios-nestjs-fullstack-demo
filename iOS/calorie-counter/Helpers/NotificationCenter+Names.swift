//
//  NotificationCenter+Names.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 31/01/2022.
//

import Foundation

extension Notification.Name {
    
    static let Logout = Notification.Name("UserLogout")
}
