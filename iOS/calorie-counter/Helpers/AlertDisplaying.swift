//
//  AlertDisplaying.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import UIKit

protocol AlertDisplayingProtocol: AnyObject {
    
    func showAlert(presentation: AlertPresentation)
}

extension AlertDisplayingProtocol where Self: UIViewController {
    
    func showAlert(presentation: AlertPresentation) {
        self.displayAlert(using: presentation)
    }
}
