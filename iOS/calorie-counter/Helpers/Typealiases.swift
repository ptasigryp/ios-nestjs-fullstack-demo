//
//  Typealiases.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import Foundation

// MARK: - Generic

typealias ReturnClosure<T> = () -> T
typealias ThrowableReturnClosure<T> = () throws -> T
typealias ValueClosure<T> = ValueReturnClosure<T, Void>
typealias ValueReturnClosure<T, R> = (_ value: T) -> R
typealias ValueThrowableReturnClosure<T, R> = (_ value: T) throws -> R
typealias VoidClosure = ReturnClosure<Void>
typealias VoidThrowableClosure = ThrowableReturnClosure<Void>

// MARK: - Networking

typealias Handler<T, E: Error> = ValueClosure<Result<T, E>>
typealias NetworkHandler<T> = ValueClosure<Result<T, Error>>
typealias EmptyNetworkHandler = NetworkHandler<Void>
