//
//  LoadingStateProtocol.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import UIKit

protocol LoadingStateProtocol: AnyObject {
    
    func setLoadingState(loading: Bool)
}

extension LoadingStateProtocol where Self: UIViewController {
    func setLoadingState(loading: Bool) {
        view.setDisplayingLoading(state: loading)
    }
}
