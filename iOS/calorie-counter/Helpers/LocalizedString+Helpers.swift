//
//  LocalizedString+Helpers.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import Foundation

func localized(_ stringKey: String) -> String {
    NSLocalizedString(stringKey, comment: "")
}
