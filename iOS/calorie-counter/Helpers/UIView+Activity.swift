//
//  UIView+Activity.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import UIKit

final class ActivityIndicatorContainer: UIView {
    
    private enum Constants {
        
        static let backgroundAlpha: CGFloat = 0.15
    }

    var isEnabled: Bool = false {
        didSet {
            isEnabled ? start() : stop()
        }
    }

    var isAnimating: Bool {
        activityIndicatorView.isRotating
    }

    private lazy var activityIndicatorView: LoadingIndicator = {
        let imageView = LoadingIndicator()
        imageView.addConstraint(imageView.widthAnchor.constraint(lessThanOrEqualToConstant: 48))
        imageView.addConstraint(imageView.heightAnchor.constraint(equalTo: imageView.heightAnchor, multiplier: 1))
        return imageView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }

    private func initialize() {
        backgroundColor = .black.withAlphaComponent(Constants.backgroundAlpha)

        addSubview(activityIndicatorView)
        activityIndicatorView.centerInSuperview()
    }

    private func start() {
        isHidden = false
        alpha = 0.0
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 1.0
        })
        activityIndicatorView.startRotating(direction: .clockwise, velocity: 1.0)
    }

    private func stop() {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0.0
        }, completion: { finished in
            guard finished else { return }
            self.isHidden = true
            self.activityIndicatorView.stopRotating()
        })
    }
}

final class LoadingIndicator: UIImageView, RotableView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    convenience init() {
        self.init(frame: .zero)
    }

    private func commonInit() {
        self.image = UIImage(named: "loadingSpinner")
        self.tintColor = .blue
    }
}

protocol LoadingDisplayable {
    
    var isDisplayingLoading: Bool { get }
    
    func setDisplayingLoading(state: Bool)
}

extension UIView: LoadingDisplayable {

    private var loadingSpinnerContainer: ActivityIndicatorContainer? {
        subviews.compactMap { $0 as? ActivityIndicatorContainer }.first
    }

    var isDisplayingLoading: Bool {
        loadingSpinnerContainer?.isAnimating ?? false
    }

    func setDisplayingLoading(state: Bool) {
        if let container = loadingSpinnerContainer {
            container.isEnabled = state
        } else {
            let container = ActivityIndicatorContainer()
            addSubview(container)
            container.fillSuperview()
            container.isEnabled = state
        }
    }
}
