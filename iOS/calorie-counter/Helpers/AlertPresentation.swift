//
//  AlertModel.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import UIKit

struct AlertButtonPresentation {
    
    typealias AlertButtonAction = (() -> Void)
    
    let title: String
    let action: AlertButtonAction?
    let style: UIAlertAction.Style
    
    init(
        title: String,
        style: UIAlertAction.Style = .default,
        action: AlertButtonAction? = nil
    ) {
        self.title = title
        self.action = action
        self.style = style
    }

    static let cancelButton: AlertButtonPresentation = .init(
        title: localized("general_cancel"),
        style: .destructive
    )
    static let okButton: AlertButtonPresentation = .init(title: localized("general_ok"))
}

struct AlertPresentation {

    let title: String
    let message: String
    let buttons: [AlertButtonPresentation]
    let style: UIAlertController.Style
}

extension AlertPresentation {
    
    static func infoAlertPresentation(
        title: String,
        message: String,
        cancelAction: (() -> Void)? = nil
    ) -> AlertPresentation {
        let okButton = AlertButtonPresentation(title: localized("general_ok"), action: cancelAction)
        return AlertPresentation(
            title: title,
            message: message,
            buttons: [okButton],
            style: .alert
        )
    }

    static func tryAgainAlertPresentation(
        title: String = localized("general_error"),
        message: String = "Something went wrong",
        cancelAction: (() -> Void)? = nil,
        tryAgainAction: @escaping () -> Void
    ) -> AlertPresentation {
        let buttons = [
            AlertButtonPresentation(title: localized("general_cancel"), style: .cancel) {
                cancelAction?()
            },
            AlertButtonPresentation(title: localized("general_cancel")) {
                tryAgainAction()
            }
        ]
        
        return AlertPresentation(
            title: title,
            message: message,
            buttons: buttons,
            style: .alert
        )
    }
    
    static func actionSheetPresentation(
        title: String,
        message: String,
        buttons: [AlertButtonPresentation]
    ) -> AlertPresentation {
        return AlertPresentation(
            title: title,
            message: message,
            buttons: buttons,
            style: .actionSheet
        )
    }
}
