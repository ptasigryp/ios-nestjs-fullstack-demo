//
//  UIViewController+Alert.swift
//  calorie-counter
//
//  Created by Michał Mańkus on 29/01/2022.
//

import UIKit

extension UIViewController {
    
    func displayAlert(using presentation: AlertPresentation, withHapticFeedback: Bool = false) {
        let alert = UIAlertController(
            title: presentation.title,
            message: presentation.message,
            preferredStyle: presentation.style
        )
        
        presentation.buttons.forEach { presentation in
            let action = UIAlertAction(
                title: presentation.title,
                style: presentation.style
            ) { action in
                presentation.action?()
            }
            alert.addAction(action)
        }
        
        if withHapticFeedback {
            let generator = UINotificationFeedbackGenerator()
            generator.prepare()
            generator.notificationOccurred(.warning)
        }
        
        present(alert, animated: true)
    }
}
