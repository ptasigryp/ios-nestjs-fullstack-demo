import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import configValidator from './config/validator';
import serverConfiguration from './config/server.config';
import databaseConfiguration from './config/database.config';
import { DatabaseModule } from './database/database.module';
import { UsersModule } from './api/user/users.module';
import { AuthenticationModule } from './api/authentication/authentication.module';
import { FoodModule } from './api/food/food.module';
import { LoggerModule } from 'nestjs-pino';

@Module({
  imports: [
    ConfigModule.forRoot({
      validationSchema: configValidator,
      validationOptions: {
        allonUnknown: true,
      },
      load: [serverConfiguration, databaseConfiguration],
      isGlobal: true,
    }),
    LoggerModule.forRoot({
      pinoHttp: {
        level: process.env.NODE_ENV !== 'production' ? 'debug' : 'info',
        transport:
          process.env.NODE_ENV !== 'production'
            ? { target: 'pino-pretty' }
            : undefined,
      },
    }),
    DatabaseModule,
    AuthenticationModule,
    UsersModule,
    FoodModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
