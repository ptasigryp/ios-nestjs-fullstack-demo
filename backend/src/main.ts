import {
  ClassSerializerInterceptor,
  Logger,
  ValidationPipe,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory, Reflector } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  const logger: Logger = new Logger('app');

  const configService: ConfigService = app.get(ConfigService);
  const port: number = configService.get('port');
  const env = configService.get('nodeEnv');

  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      transformOptions: {
        enableImplicitConversion: true,
      },
    }),
  );
  app.useGlobalInterceptors(new ClassSerializerInterceptor(app.get(Reflector)));

  if (env === 'dev') {
    const config = new DocumentBuilder()
      .setTitle('Calorie Counter')
      .setDescription('Backend API documentation')
      .setVersion('1.0')
      .addBearerAuth()
      .build();
    const path = 'docs';
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup(path, app, document);

    logger.log(`* Swagger available on /${path}`);
  }

  await app.listen(port, () => {
    logger.log(`* Listening on ::${port}`);
  });
}
bootstrap();
