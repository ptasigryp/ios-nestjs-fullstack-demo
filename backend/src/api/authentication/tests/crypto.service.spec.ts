import { Test } from '@nestjs/testing';
import { CryptoService } from '../crypto.service';

describe('The CryptoService', () => {
  let cryptoService: CryptoService;
  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [CryptoService],
    }).compile();

    cryptoService = await module.get(CryptoService);
  });

  describe('when comparing passwords', () => {
    it('should pass as valid', async () => {
      const result = await cryptoService.verifyPassword(
        'random_password',
        '$2b$10$JgkbYn0Ew2dhDSEBd8EuG.sMeslsjN8B5gZSrrjEIIO1.WDbMmSGO',
      );
      expect(result).toBe(true);
    });

    it('should pass as invalid', async () => {
      const result = await cryptoService.verifyPassword(
        'random_password',
        '$2y$10$2d8eEOa3EgreCRZkqH4lg.ei29UZnQyxr0dDEP1W1mro6jjVsXfIO',
      );
      expect(result).toBe(false);
    });
  });
});
