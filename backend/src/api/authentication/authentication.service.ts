import RegisterDto from './dto/register.dto';
import {
  UserAlreadyExistsException,
  WrongCredentialsException,
} from '../constants/exceptions';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { Injectable } from '@nestjs/common';
import PostgresErrorCode from '../../database/postgresErrorCode.enum';
import { CryptoService } from './crypto.service';
import { UsersService } from '../user/users.service';
import { TokenPayload } from './authentication.types';

@Injectable()
export class AuthenticationService {
  constructor(
    private readonly cryptoService: CryptoService,
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
  ) {}

  public async register(registrationData: RegisterDto) {
    const hashedPassword = await this.cryptoService.hashPassword(
      registrationData.password,
    );
    try {
      const createdUser = await this.usersService.create({
        ...registrationData,
        password: hashedPassword,
      });
      createdUser.password = undefined;

      return createdUser;
    } catch (error) {
      if (error?.code == PostgresErrorCode.UniqueViolation) {
        throw new UserAlreadyExistsException();
      }
      throw error;
    }
  }

  public async getAuthenticatedUser(
    username: string,
    plainTextPassword: string,
  ) {
    try {
      const user = await this.usersService.getByUsernameOrFail(username);
      const passwordMatching = await this.cryptoService.verifyPassword(
        plainTextPassword,
        user.password,
      );
      if (!passwordMatching) {
        throw new WrongCredentialsException();
      }
      user.password = undefined;
      return user;
    } catch (error) {
      throw new WrongCredentialsException();
    }
  }

  public generateJwtToken(userId: string) {
    const payload: TokenPayload = { userId };
    const token = this.jwtService.sign(payload);

    return token;
  }
}
