import { ApiProperty } from '@nestjs/swagger';

export class AuthTokenResponseInterface {
  @ApiProperty()
  readonly token: string;
}
