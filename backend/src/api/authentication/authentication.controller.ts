import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import {
  ApiResponse,
  ApiCreatedResponse,
  ApiBody,
  ApiTags,
} from '@nestjs/swagger';
import RegisterDto from './dto/register.dto';
import { LocalAuthenticationGuard } from './guards/local.guard';
import User from '../user/user.entity';
import { ExtractUser } from '../../decorators/user.decorator';
import { AuthenticationService } from './authentication.service';
import LoginDto from './dto/login.dto';
import { AuthTokenResponseInterface } from './interfaces/authTokenResponse.interface';

@ApiTags('auth')
@Controller('auth')
export class AuthenticationController {
  constructor(private readonly authenticationService: AuthenticationService) {}

  @ApiCreatedResponse({
    status: 200,
    description: 'The user has been successfully registered.',
    type: AuthTokenResponseInterface,
  })
  @ApiResponse({
    status: 400,
    description: 'User already exists',
  })
  @Post('register')
  async register(
    @Body() data: RegisterDto,
  ): Promise<AuthTokenResponseInterface> {
    const user = await this.authenticationService.register(data);
    const token = this.authenticationService.generateJwtToken(user.id);

    return {
      token,
    };
  }

  @ApiBody({ type: LoginDto })
  @ApiResponse({
    status: 200,
    description: 'User has been logged in.',
    type: AuthTokenResponseInterface,
  })
  @UseGuards(LocalAuthenticationGuard)
  @Post('login')
  async login(@ExtractUser() user: User): Promise<AuthTokenResponseInterface> {
    const token = this.authenticationService.generateJwtToken(user.id);
    return {
      token,
    };
  }
}
