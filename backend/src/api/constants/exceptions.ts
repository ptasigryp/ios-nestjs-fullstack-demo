import { HttpException, HttpStatus } from '@nestjs/common';

export class InternalServerException extends HttpException {
  constructor(message?: string) {
    super(
      {
        message: message ?? ' Something went wrong',
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        error: 'InternalServerError',
      },
      HttpStatus.INTERNAL_SERVER_ERROR,
    );
  }
}

export class NotFoundException extends HttpException {
  constructor(message?: string) {
    super(
      {
        message: message ?? 'Not found',
        status: HttpStatus.NOT_FOUND,
        error: 'NotFoundError',
      },
      HttpStatus.NOT_FOUND,
    );
  }
}

export class WrongCredentialsException extends HttpException {
  constructor() {
    super(
      {
        message: 'Wrong credentials',
        status: HttpStatus.BAD_REQUEST,
        error: 'WrongCredentialsError',
      },
      HttpStatus.BAD_REQUEST,
    );
  }
}

export class UserNotFoundException extends NotFoundException {
  constructor() {
    super('User does not exists');
  }
}

export class UserAlreadyExistsException extends HttpException {
  constructor() {
    super(
      {
        message: 'User already exists',
        status: HttpStatus.BAD_REQUEST,
        error: 'UserAlreadyExistsError',
      },
      HttpStatus.BAD_REQUEST,
    );
  }
}

export class NoPermissionException extends HttpException {
  constructor(message?: string) {
    super(
      {
        message: message ?? 'No permission',
        status: HttpStatus.FORBIDDEN,
        error: 'NoPermissionError',
      },
      HttpStatus.FORBIDDEN,
    );
  }
}
