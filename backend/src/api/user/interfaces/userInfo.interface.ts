import { ApiProperty } from '@nestjs/swagger';

export default class UserInfoInterface {
  @ApiProperty()
  readonly id: string;

  @ApiProperty()
  readonly username: string;

  @ApiProperty()
  readonly isAdmin: boolean;
}
