import { Controller, Get, UseGuards } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { ExtractUser } from 'src/decorators/user.decorator';
import { JwtAuthenticationGuard } from '../authentication/guards/jwtAuth.guard';
import UserInfoInterface from './interfaces/userInfo.interface';
import User from './user.entity';
import { UsersService } from './users.service';

@ApiTags('user')
@Controller('user')
export default class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @ApiResponse({
    status: 200,
    description: 'Retrieved user info.',
    type: UserInfoInterface,
  })
  @Get('/info')
  @UseGuards(JwtAuthenticationGuard)
  async getUserInfo(@ExtractUser() user: User): Promise<UserInfoInterface> {
    const result: UserInfoInterface = {
      id: user.id,
      username: user.username,
      isAdmin: user.isAdmin(),
    };
    return result;
  }
}
