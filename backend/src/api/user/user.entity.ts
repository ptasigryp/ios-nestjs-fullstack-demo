import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  Entity,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  OneToMany,
} from 'typeorm';
import FoodEntry from '../food/food.entity';

@Entity()
class User {
  @ApiProperty()
  @PrimaryColumn()
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @ApiProperty()
  @Column({ unique: true })
  public username: string;

  @Column()
  public password: string;

  @ApiProperty()
  @Column({ default: false })
  public admin: boolean;

  @OneToMany(() => FoodEntry, (entry: FoodEntry) => entry.creator)
  public foodEntries?: FoodEntry[];

  public isAdmin(): boolean {
    return this.admin === true;
  }
}

export default User;
