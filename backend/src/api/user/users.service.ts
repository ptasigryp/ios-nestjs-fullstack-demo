import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserNotFoundException } from '../constants/exceptions';
import CreateUserDto from './dto/createUser.dto';
import User from './user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async getByUsername(username: string): Promise<User | null> {
    const user = await this.usersRepository.findOne({ username });
    return user;
  }

  async getByUsernameOrFail(username: string): Promise<User> {
    const user = await this.getByUsername(username);
    if (!user) {
      throw new UserNotFoundException();
    }
    return user;
  }

  async getById(id: string): Promise<User | null> {
    const user = await this.usersRepository.findOne({ id });
    return user;
  }

  async getByIdOrFail(id: string): Promise<User> {
    const user = await this.getById(id);
    if (!user) {
      throw new UserNotFoundException();
    }
    return user;
  }

  async create(userData: CreateUserDto) {
    const newUser = this.usersRepository.create(userData);
    await this.usersRepository.save(newUser);
    return newUser;
  }
}
