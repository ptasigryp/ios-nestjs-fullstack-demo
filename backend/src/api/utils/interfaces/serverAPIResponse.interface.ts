interface ServerAPIResponse<T> {
  readonly result: T;
}

export default ServerAPIResponse;
