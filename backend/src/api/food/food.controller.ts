import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import FoodService from './food.service';
import { ApiTags, ApiResponse, ApiBody } from '@nestjs/swagger';
import CreateFoodEntryDto from './dto/createFoodEntry.dto';
import { ExtractUser } from 'src/decorators/user.decorator';
import User from '../user/user.entity';
import FoodEntry from './food.entity';
import CreateFoodEntryData from './dto/createFoodEntryData.dto';
import { JwtAuthenticationGuard } from '../authentication/guards/jwtAuth.guard';
import ServerAPIResponse from '../utils/interfaces/serverAPIResponse.interface';
import { FoodEntryResultInterface } from './interfaces/foodEntryResult.interface';
import {
  NoPermissionException,
  NotFoundException,
} from '../constants/exceptions';
import UpdateFoodEntryDto from './dto/updateFoodEntry.dto';

@ApiTags('food-entries')
@Controller('food-entries')
export default class FoodController {
  constructor(private readonly foodService: FoodService) {}

  @ApiBody({ type: CreateFoodEntryDto })
  @ApiResponse({
    status: 200,
    description: 'Creates food entry.',
    type: FoodEntry,
  })
  @Post()
  @UseGuards(JwtAuthenticationGuard)
  async createFoodEntry(
    @Body() payload: CreateFoodEntryDto,
    @ExtractUser() user: User,
  ): Promise<FoodEntryResultInterface> {
    const entryData: CreateFoodEntryData = {
      name: payload.name,
      createdAt: payload.date,
      calories: payload.calories,
      price: payload.price,
      creator: user,
    };
    const entry = await this.foodService.create(entryData);
    return {
      id: entry.id,
      creatorId: entry.creatorId,
      creatorName: user.username,
      name: entry.name,
      calories: entry.calories,
      price: entry.price,
      createdAt: entry.createdAt,
    };
  }

  @ApiBody({ type: UpdateFoodEntryDto })
  @ApiResponse({
    status: 200,
    description: 'Updates food entry.',
    type: FoodEntry,
  })
  @Patch(':id')
  @UseGuards(JwtAuthenticationGuard)
  async updateFoodEntry(
    @Param('id') id: string,
    @Body() payload: UpdateFoodEntryDto,
    @ExtractUser() user: User,
  ): Promise<FoodEntryResultInterface> {
    if (!user.isAdmin()) {
      throw new NoPermissionException();
    }

    const entry = await this.foodService.findOne(id);
    if (!entry) {
      throw new NotFoundException();
    }

    entry.createdAt = payload.date ?? entry.createdAt;
    entry.name = payload.name;
    entry.calories = payload.calories;
    entry.price = payload.price;

    await this.foodService.update(entry);

    return {
      id: entry.id,
      creatorId: entry.creatorId,
      creatorName: user.username,
      name: entry.name,
      calories: entry.calories,
      price: entry.price,
      createdAt: entry.createdAt,
    };
  }

  @ApiResponse({
    status: 200,
    description: 'Retrieved all food entries.',
    type: [FoodEntryResultInterface],
  })
  @Get()
  @UseGuards(JwtAuthenticationGuard)
  async getFoodEntries(
    @ExtractUser() user: User,
  ): Promise<ServerAPIResponse<FoodEntryResultInterface[]>> {
    const whereCondition: Record<string, any> = {};
    if (!user.isAdmin()) {
      whereCondition['creatorId'] = user.id;
    }
    const entries: FoodEntry[] = await this.foodService.findMany(
      {
        createdAt: 'DESC',
      },
      whereCondition,
      ['creator'],
    );

    const result: FoodEntryResultInterface[] = entries.map((entry) => {
      return {
        id: entry.id,
        creatorId: entry.creatorId,
        creatorName: entry.creator.username,
        name: entry.name,
        calories: entry.calories,
        price: entry.price,
        createdAt: entry.createdAt,
      };
    });
    return { result };
  }

  @ApiResponse({
    status: 200,
    description: 'Removed food entry.',
    type: FoodEntry,
  })
  @ApiResponse({
    status: 403,
    description: 'No permission.',
  })
  @ApiResponse({
    status: 404,
    description: 'Not found.',
  })
  @Delete(':id')
  @UseGuards(JwtAuthenticationGuard)
  async removeFoodEntry(@Param('id') id: string, @ExtractUser() user: User) {
    if (!user.isAdmin()) {
      throw new NoPermissionException();
    }

    const entry = await this.foodService.findOne(id);
    if (!entry) {
      throw new NotFoundException();
    }
    await this.foodService.remove(entry);
    return { result: entry };
  }
}
