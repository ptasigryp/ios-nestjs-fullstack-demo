import User from 'src/api/user/user.entity';

export class CreateFoodEntryData {
  creator: User;
  createdAt: Date;
  name: string;
  calories: number;
  price?: number;
}

export default CreateFoodEntryData;
