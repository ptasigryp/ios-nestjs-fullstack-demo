import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class FoodEntryResultInterface {
  @ApiProperty()
  readonly id: string;

  @ApiProperty()
  readonly creatorId: string;

  @ApiProperty()
  readonly creatorName: string;

  @ApiProperty()
  readonly name: string;

  @ApiProperty()
  readonly calories: number;

  @ApiPropertyOptional()
  readonly price?: number;

  @ApiProperty()
  readonly createdAt: Date;
}
