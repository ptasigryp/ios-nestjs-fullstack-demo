import { ApiProperty } from '@nestjs/swagger';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  Index,
  ManyToOne,
  PrimaryColumn,
} from 'typeorm';
import User from '../user/user.entity';

@Entity()
class FoodEntry {
  @ApiProperty()
  @PrimaryColumn()
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @ManyToOne(() => User, (creator: User) => creator.foodEntries)
  public creator: User;

  @Column({ nullable: true })
  @Index()
  public creatorId: string;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  public createdAt: Date;

  @ApiProperty()
  @Column()
  public name: string;

  @ApiProperty()
  @Column()
  public calories: number;

  @ApiProperty()
  @Column({ nullable: true, type: 'float' })
  public price?: number;
}

export default FoodEntry;
