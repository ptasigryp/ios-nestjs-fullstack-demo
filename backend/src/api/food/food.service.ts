import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import FoodEntry from './food.entity';
import CreateFoodEntryData from './dto/createFoodEntryData.dto';

@Injectable()
export default class FoodService {
  constructor(
    @InjectRepository(FoodEntry)
    private foodRepository: Repository<FoodEntry>,
  ) {}

  async findMany(
    order?: Record<string, string>,
    where?: Record<string, any>,
    loadRelations?: string[],
  ): Promise<FoodEntry[]> {
    return this.foodRepository.find({
      order: order,
      where: where,
      relations: loadRelations,
    });
  }

  async findOne(id: string): Promise<FoodEntry> {
    return this.foodRepository.findOne(id);
  }

  async create(payload: CreateFoodEntryData): Promise<FoodEntry> {
    const entry = this.foodRepository.create(payload);
    await this.foodRepository.save(entry);
    return entry;
  }

  async update(entry: FoodEntry): Promise<void> {
    await this.foodRepository.update({ id: entry.id }, entry);
  }

  async remove(entry: FoodEntry): Promise<void> {
    await this.foodRepository.remove(entry);
  }
}
