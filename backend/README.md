# Calorie Counter Backend

## Getting started
```bash
# 1. Enter your project directory
$ cd backend

# 3. Configure environment
$ cp .env.example .env
$ cp docker.env-example docker.env

# 3. Install dependencies
$ npm i
```

## Running
```bash
# 1. Initialize docker containers
$ docker-compose up -d

# 2. Run your API
# watch mode
$ npm run start:dev
# production mode
$ npm run start:prod

# App should be available on http://localhost:8000
```

## Features
- JWT Authentication strategy

## Documentation
- Swagger documentation is available at http://localhost:8000/docs

## Testing

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
