# Calories counter
## About

Calorie counter application for iOS. This is demo app that allows to track your food, calories of it and budget.

## Technology stack

- Swift 5.5
- TypeScript
- Node.js with Nest.js framework
- PostgreSQL

## Requirements

- XCode
- Node.js
- CocoaPods
- Docker

## Architecture

### iOS

The project uses MVVM architecture with addition of Coordinators.

![](resources/ios-architecture.jpg)

### Backend

Stanard Nest.js modular architecture similiar to Angular.

![](resources/nest-architecture.png)

## Video demo

![](resources/calorie-counter.mp4)
